﻿# Ubiquiti Home IPv4 and IPv6 Firewall Project

## Table of Content
[1. Overview](#1-overview) <br>
[2. Requirements](#2-requirements) <br>
[3. Home Network Topology](#3-home-network-topology) <br>
[4. Connect your PC’s Unifi Controller software to the USG](#4-connect-your-pcs-unifi-controller-software-to-the-usg) <br>
[5. Activate the USG LAN2](#5-activate-the-usg-lan2) <br>
[6. Migrate the USG from your local PC Unifi Controller to your Cloud Key Unifi Controller](#6-migrate-the-usg-from-your-local-pc-unifi-controller-to-your-cloud-key-unifi-controller) <br>
&nbsp;&nbsp;&nbsp;[Optional Steps, if you can’t see the USG in the Devices section](#optional-steps-if-you-cant-see-the-usg-in-the-devices-section) <br>
[7. Setup Your Unifi Access Point](#7-setup-your-unifi-access-point) <br>
[8. Setup Your NetGear DOCSIS Modem from Router Mode to Bridge Mode](#8-setup-your-netgear-docsis-modem-from-router-mode-to-bridge-mode) <br>
[9. Manage Your Cloud Key Controller Using Your Mobile App](#9-manage-your-cloud-key-controller-using-your-mobile-app) <br>
[10. Setup IPv6 in Your Home Network](#10-setup-ipv6-in-your-home-network) <br>
[11. Setup 4 More SSIDs](#11-setup-4-more-ssids) <br>
&nbsp;&nbsp;&nbsp;[11.1 Backup Your Current Controller & USG Settings](#111-backup-your-current-controller-usg-settings) <br>
&nbsp;&nbsp;&nbsp;[11.2 The Steps To Create A New SSID Associated With An IPv6 Network Only Using DHCPv6-PD](#112-the-steps-to-create-a-new-ssid-associated-with-an-ipv6-network-only-using-dhcpv6-pd) <br>
&nbsp;&nbsp;&nbsp;[11.3 The Steps To Create A New SSID Associated With An IPv6 Network Only Using NPTv6](#113-the-steps-to-create-a-new-ssid-associated-with-an-ipv6-network-only-using-nptv6) <br>
&nbsp;&nbsp;&nbsp;[11.4 The Steps To Create A New Guest SSID Associated With IPv4 Subnet Only](#114-the-steps-to-create-a-new-guest-ssid-associated-with-ipv4-subnet-only) <br>
[12. Setup The Firewall Policy to Secure Your Deployment](#12-setup-the-firewall-policy-to-secure-your-deployment) <br>
[13. Restart Your USG To Ensure The Setting Is Permanent, Even After Power Failure](#13-restart-your-usg-to-ensure-the-setting-is-permanent-even-after-power-failure) <br>
[14. How To Capture Your Specific Wireless Device Traffic Using The USG](#14-how-to-capture-your-specific-wireless-device-traffic-using-the-usg) <br>
[15. Setup Pi-Hole in Raspberry Pi to Get A Secure Home DNS Server](#15-setup-pi-hole-in-raspberry-pi-to-get-a-secure-home-dns-server) <br>
&nbsp;&nbsp;&nbsp;[15.1. Pi-Hole Installation](#151-pi-hole-installation) <br>
&nbsp;&nbsp;&nbsp;[15.2. How to Run Another Webserver While Using Pi-Hole](#152-how-to-run-another-webserver-while-using-pi-hole) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1. Step 1 - Set A Secondary Static IP](#1-step-1-set-a-secondary-static-ip) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1.1. Primary Static IP](#11-primary-static-ip) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1.2. Secondary Static IP](#12-secondary-static-ip) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[2. Step 2 - Bind Lighttpd To The Primary IP](#2-step-2-bind-lighttpd-to-the-primary-ip) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[3. Step 3 - Bind Your Web Server To The Secondary IP](#3-step-3-bind-your-web-server-to-the-secondary-ip) <br>
[16. Setup Cacti in The Raspberry Pi to Get A Network Monitoring Server](#16-setup-cacti-in-the-raspberry-pi-to-get-a-network-monitoring-server) <br>
[17. Setup A Static Website In Another Raspberry Pi](#17-setup-a-static-website-in-another-raspberry-pi) <br>
&nbsp;&nbsp;&nbsp;[17.1. Create A New Local Network & SSID Associated With an IPv4 & IPv6 Network Using DHCPv6-PD](#171-create-a-new-local-network-ssid-associated-with-an-ipv4-ipv6-network-using-dhcpv6-pd) <br>
&nbsp;&nbsp;&nbsp;[17.2. Setup The Testing Web Page, Using the Budiman 5 SSID](#172-setup-the-testing-web-page-using-the-budiman-5-ssid) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[2. Edit dhcpcd.conf](#2-edit-dhcpcdconf) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[3. Reboot The Raspberry Pi, To Bring The IP Configuration Into Effect](#3-reboot-the-raspberry-pi-to-bring-the-ip-configuration-into-effect) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[4. Setup The Testing Page](#4-setup-the-testing-page) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[5. How To Make Your Apache Server More Secure](#5-how-to-make-your-apache-server-more-secure) <br>
[18. Setup A Remote User VPN Using OpenVPN](#18-setup-a-remote-user-vpn-using-openvpn) <br>
&nbsp;&nbsp;&nbsp;[18.1. Set Up SSH Keys In Both Raspberry Pis](#181-set-up-ssh-keys-in-both-raspberry-pis) <br>
&nbsp;&nbsp;&nbsp;[18.2. Setup CA Server and OpenVPN Server In 2 Different Pis](#182-setup-ca-server-and-openvpn-server-in-2-different-pis) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1. Step 1 - Installing OpenVPN and EasyRSA](#1-step-1-installing-openvpn-and-easyrsa) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[2. Step 2 - Configuring the EasyRSA Variables and Building the CA](#2-step-2-configuring-the-easyrsa-variables-and-building-the-ca) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[3. Step 3 - Creating the Server Certificate, Key, and Encryption Files](#3-step-3-creating-the-server-certificate-key-and-encryption-files) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[4. Step 4 - Generating a Client Certificate and Key Pair](#4-step-4-generating-a-client-certificate-and-key-pair) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[5. Step 5 - Configuring the OpenVPN Service](#5-step-5-configuring-the-openvpn-service) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[6. Step 6 - Adjusting the Server Networking Configuration](#6-step-6-adjusting-the-server-networking-configuration) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[7. Step 7 - Starting and Enabling the OpenVPN Service](#7-step-7-starting-and-enabling-the-openvpn-service) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[8. Step 8 - Creating the Client Configuration Infrastructure](#8-step-8-creating-the-client-configuration-infrastructure) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[9. Step 9 - Generating Client Configurations](#9-step-9-generating-client-configurations) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[10. Step 10 - Installing the Client Configuration](#10-step-10-installing-the-client-configuration) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[10.1. Windows](#101-windows) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[A. Installing in Windows](#a-installing-in-windows) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[B. Connecting in Windows](#b-connecting-in-windows) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[10.2. iOS](#102-ios) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[A. Installing in iOS](#a-installing-in-ios) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[B. Connecting in iOS](#b-connecting-in-ios) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[11. Setup NAT On The OpenVPN Server](#11-setup-nat-on-the-openvpn-server) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[11.1. Install A Firewall In The OpenVPN Server](#111-install-a-firewall-in-the-openvpn-server) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[11.2. Find The Public Network Interface Of Your Machine](#112-find-the-public-network-interface-of-your-machine) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[11.3. Open The /etc/ufw/before.rules](#113-open-the-etcufwbeforerules) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[11.4. Tell UFW To Allow Forwarded Packets By Default](#114-tell-ufw-to-allow-forwarded-packets-by-default) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[11.5. Adjust The UFW To Allow Traffic To OpenVPN](#115-adjust-the-ufw-to-allow-traffic-to-openvpn) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[11.6. Disable And Re-enable UFW](#116-disable-and-re-enable-ufw) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[11.7. Test Connect The OpenVPN Server From Local LAN And Do Traceroute To A Public Server](#117-test-connect-the-openvpn-server-from-local-lan-and-do-traceroute-to-a-public-server) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[12. Edit The vpnclient1-full.ovpn](#12-edit-the-vpnclient1-fullovpn) <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[13. Configure Split Tunnel/Partial VPN Routing](#13-configure-split-tunnelpartial-vpn-routing) <br>
[19. Modify The USG Firewall Rules To Allow More Ports](#19-modify-the-usg-firewall-rules-to-allow-more-ports) <br>
&nbsp;&nbsp;&nbsp;[19.1. Enable Port Forwarding To The Apache-pi TCP Port 80](#191-enable-port-forwarding-to-the-apache-pi-tcp-port-80) <br>
&nbsp;&nbsp;&nbsp;[19.2. Add IPv6 Firewall Rules To Allow IPv6 tcp/80 To The Apache-pi](#192-add-ipv6-firewall-rules-to-allow-ipv6-tcp80-to-the-apache-pi) <br>
&nbsp;&nbsp;&nbsp;[19.3. Enable Port Forwarding To The Apache-pi UDP Port 1194 (OpenVPN)](#193-enable-port-forwarding-to-the-apache-pi-udp-port-1194-openvpn) <br>
&nbsp;&nbsp;&nbsp;[19.4. Add IPv6 Firewall Rules To Allow IPv6 ICMP To The Apache-pi](#194-add-ipv6-firewall-rules-to-allow-ipv6-icmp-to-the-apache-pi) <br>
&nbsp;&nbsp;&nbsp;[19.5. Add IPv6 Firewall Rules To Allow IPv6 ICMP To The Pi-Hole](#195-add-ipv6-firewall-rules-to-allow-ipv6-icmp-to-the-pi-hole) <br>
[20. Setup DNS In Google Domains](#20-setup-dns-in-google-domains) <br>
[21. How To Setup HTTPS On Your Webpage](#21-how-to-setup-https-on-your-webpage) <br>
&nbsp;&nbsp;&nbsp;[21.1. Enable Port Forwarding To The Apache-pi TCP Port 443](#211-enable-port-forwarding-to-the-apache-pi-tcp-port-443) <br>
&nbsp;&nbsp;&nbsp;[21.2. Add IPv6 Firewall Rules To Allow IPv6 tcp/443 To The Apache-pi](#212-add-ipv6-firewall-rules-to-allow-ipv6-tcp443-to-the-apache-pi) <br>
&nbsp;&nbsp;&nbsp;[21.3. Let’s Encrypt Setup](#213-lets-encrypt-setup) <br>
[22. Install Fail2Ban](#22-install-fail2ban) <br>
[23. Backup Both Raspberry Pis' Configuration Once A Week](#23-backup-both-raspberry-pis-configuration-once-a-week) <br>
[24. References](#24-references) <br>

## 1. Overview
This document tries to help the reader to setup a home firewall with IPv6, WiFi, and Guest features using Ubiquiti solutions. After this setup has been completed, the reader should be able to test which web or mobile applications in the internet have supported IPv6, and also which IoT devices at home have supported IPv6.

## 2. Requirements

| No  | Device  | Price (amazon.com), Dec 2019  |
| -------------  | -------------  | -------------  |
| 1  | Ubiquiti Unifi Security Gateway (USG)  | US$ 127.94  |
| 2  | Ubiquiti UniFi Cloud Key (UC-CK)  | US$ 96.36  |
| 3  | Ubiquiti Unifi Ap-AC Lite - Wireless Access Point - 802.11 B/A/G/n/AC (UAP AC LITE US), White  | US$ 78.58  |
| 4  | 2x Raspberry Pi 3 B+ with Case, Power Supply Kit, and 32GB MicroSD to run Pi-Hole + 2x microSD card  | US$ 88  |
| 5  | Annual LastPass Family password manager plan  | US$ 48  |
| 6  | Annual Google Domain name  | US$ 12  |
|   | Total  | US$ 450.88 (tax excluded)  |
| 7  | UniFi US-8-60W 8-Port Gigabit PoE Switch (Optional)  | US$ 115  |
|   | Total (Optional)  | US$ 565.88 (tax excluded)  |

## 3. Home Network Topology
Connect the network devices as per the below network topology.
<img src="images/03-topology.png" width="500">

This setup doesn’t need a big space. Both Raspberry Pis and all the clients are connected via wireless. Adding a network switch will require a bigger space. <br>
<img src="images/03-setup01.png" width="250">&nbsp;
<img src="images/03-setup02.png" width="300">&nbsp;
<img src="images/03-setup03.png" width="250">

## 4. Connect your PC’s Unifi Controller Software to The USG
1. Create a Ubiquiti account at https://account.ui.com/register. <br>

2. Download & install the Unifi Controller software from https://www.ui.com/download/unifi. <br>

3. Launch the Unifi Controller software. <br>
   Click the ‘Launch a Browser to Manage the Network’ button. <br>

4. Login the the Ubiquiti account. <br>
   The browser then should open https://localhost:8443/manage/site/default/dashboard. <br>

5. Download the latest USG firmware from here: https://www.ui.com/download/unifi-switching-routing/usg/usg. <br>
   Then copy the latest firmware URL, for example https://dl.ui.com/unifi/firmware/UGW3/4.4.44.5213844/UGW3.v4.4.44.5213844.tar. <br>

6. SSH into 192.168.1.1 (the USG), login with **ubnt/ubnt** <br>
   Do this CLI command: **sudo vi /usr/bin/ubnt-upgrade** <br>

   Scroll down to line 504 <br>
   Hold **shift+i** to enter input mode in Vi <br>
   Add a **#** in front of line 504. Like this: #exit 1 <br>
   Press **esc** followed by **:wq**, to save and close the file <br>
   Proceed with the upgrade with the below command, which should now finish without errors. <br>

   **upgrade https://dl.ui.com/unifi/firmware/UGW3/4.4.44.5213844/UGW3.v4.4.44.5213844.tar** <br>

   Note that the text *"Upgrade image does not support the device. Upgrade failed."* will be displayed, but the update will continue anyway. At the time of this writing, UGW3.v4.4.44.5213844.tar is the latest code. You should upgrade to the latest code. <br>

7. Using a web browser, browse to the USG: https://192.168.1.1/#/login , login with **ubnt/ubnt**. <br>

8. from the USG dashboard, go to Configuration > Controller Settings, then fill in the below fields: <br>
WAN Settings <br>
  &nbsp;&nbsp;&nbsp;Connection Type: DHCP <br>
  &nbsp;&nbsp;&nbsp;Preferred DNS: 75.75.75.75 <br>
  &nbsp;&nbsp;&nbsp;Alternate DNS: 75.75.76.76 <br>
LAN Settings <br>
  &nbsp;&nbsp;&nbsp;IP Address: 192.168.1.1 <br>
  &nbsp;&nbsp;&nbsp;Subnet Mask: 255.255.255.0 <br>
  &nbsp;&nbsp;&nbsp;DHCP Server: ON <br>
  &nbsp;&nbsp;&nbsp;DHCP Range: 192.168.1.6 – 192.168.1.254 <br>
Then click ‘Apply’. <br>
<img src="images/04-01-controller-setting.png" width="700"> <br>
When successful, you will get the below message: <br>
<img src="images/04-02-congrats.png" width="500"> <br>

9. (Optional, if the USG is not connected to the Unifi Controller at your PC) <br>
   SSH into 192.168.1.1, login with **ubnt/ubnt** <br>
   Do the below CLI command: <br>

   **set-inform http://192.168.1.5:8080/inform (if 192.168.1.5 is your PC’s IP address)** <br>

   OR <br>
   On the ‘Inform URL’ field, enter: http://192.168.1.5:8080/inform,  <br>
   If 192.168.1.5 is your PC’s IP address. <br>
   Then click ‘Apply Changes’. <br>
   <img src="images/04-03-inform-url.png" width="700"> <br>

10. If the USG is connected, you should be able to see the ‘Open the Unifi Controller’ icon on the top right. <br>
<img src="images/04-04-open-unifi-controller-icon.png" width="800"> <br>
    Click the icon. <br>

11. Login to the Unifi Controller, then click the ‘Devices’ icon on the left. If it’s successful, you should be able to see the ‘Connected’ status on the left. <br>
<img src="images/04-05-controller-devices.png" width="800"> <br>

## 5. Activate the USG LAN2
You can skip this step if you have a network switch at home. Since I don’t want to spend another US$ 115 for a ‘UniFi US-8-60W 8-Port Gigabit PoE Switch’, I need to activate the USG’s LAN2 port to connect the Cloud Key there. <br>
1. From the Unifi Controller Dashboard, click the ‘Settings’ icon on the left, then select ‘Networks > Local Networks’. Then click the ‘Create New Local Network’ button. <br>
   <img src="images/05-01-create-local-network.png" width="700"> <br>

2. Select the ‘Create Advanced Network’. <br>

3. Then fill in the fields below: <br>
   Network Name: LAN2 <br>
   Network Purpose: Corporate <br>
   Network Group: LAN2 <br>
   Network Size: Small LAN (253 Clients) <br>
   Gateway IP / Subnet: 192.168.2.1/24 <br>
   DHCP Range: 192.168.2.6 - 192.168.2.254 <br>
   Click the ‘Done’ button at the bottom. <br>

4. If successful, you will see the LAN2 created: <br>
<img src="images/05-02-lan2-created.png" width="800"> <br>

## 6. Migrate the USG from your local PC Unifi Controller to your Cloud Key Unifi Controller
1. Connect your Cloud Key to your PC using a cross-over cable. <br>

2. Using web browser, browse to the Cloud Key device IP: https://192.168.1.30. <br>

3. Select the ‘Configure your Unifi Cloud Key’ option. <br>
<img src="images/06-01-configure-cloud-key.png" width="500"> <br>

4. Login using ubnt/ubnt <br>

5. change the password <br>

6. In the Cloud Key Dashboard, go to Configuration > Network Settings, select Configuration Mode to Static, then fill in the fields below: <br>
  IP address: 192.168.2.5 <br>
  Netmask: 255.255.255.0 <br>
  Gateway: 192.168.2.1 <br>
  Primary DNS: 75.75.75.75 <br>
  Secondary DNS: 75.75.76.76 <br>
Then click ‘Apply Changes’. <br>
<img src="images/06-02-apply-changes.png" width="700"> <br>

7. Connect the Cloud Key to the USG port LAN2, and your PC to the USG port LAN1 <br>

8. From a web browser, browse https://192.168.2.5/ <br>

9. Select ‘Manage your Devices by Unifi Controller’. <br>

10. Give a name to your Controller. E.g. ‘Unifi Network’ <br>

11. Sign in with your Ubiquiti account. <br>

12. Configure the following setup, e.g. Wifi Name, password, etc. Then click Finish. <br>
<img src="images/06-03-manage-controller.png" width="500"> <br>

13. Don't forget to repeat the steps in section B above. If you forget, then the connectivity to the unifi cloud key will be lost. This step is **VERY IMPORTANT**. <br>

14. in the Cloud Key dashboard, select ‘Devices’ on the left, then you should be able to see the USG with the ‘Managed by Other’ status. <br>
<img src="images/06-04-usg-managed-by-other.png" width="300"> <br>

### (Optional Steps, if you can’t see the USG in the Devices section)

15. SSH into 192.168.1.1, login with the USG SSH credential. <br>

16. Do this CLI command: **set-inform http://192.168.2.5:8080/inform** (192.168.2.5 is your Cloud Key’s IP address) <br>

17. Click the USG, then click the ‘+’ icon (Adopt) on the right. <br>
<img src="images/06-05-adopt-usg.png" width="800"> <br>

18. In the Advanced Adopt section, fill in the username & password fields with the USG credential, then click Adopt. <br>
<img src="images/06-06-adopt-usg2.png" width="300"> <br>

19. If the credential is correct, then the USG status will be changed to ‘Provisioning’, then ‘Connected’.
<img src="images/06-07-usg-provisioning.png" width="300">&nbsp;
<img src="images/06-08-usg-connected.png" width="300"> <br>

20. Congratulations! Now the Cloud Key is the Unifi Controller of your USG. <br>

(Optional educational information) <br>
21. To find the SSH credential,  <br>
a. go to Settings > Site > Services, then tick (v) Enable Advanced features, then <br>

b. go to Settings > Site > Device Authentication, then show the SSH password. <br>
<img src="images/06-09-find-usg-ssh-credential.png" width="700"> <br>

22. To view the USG’s ARP Table, SSH into 192.168.1.1, login with the above SSH credential, then issue ‘show arp’. <br>

## 7. Setup Your Unifi Access Point
1. Ensure the Wireless Configuration has been provisioned. <br>
   Go to Settings > Wireless Networks <br>

2. Since the LAN1 port will be used by the AP, manage the Unifi Controller from the Unifi Cloud Account. From a web browser, browse to https://unifi.ubnt.com , then login. <br>

3. Click the ‘Launch’ button on the right of your Cloud Key. <br>

4. Connect your AP to the USG port LAN1. <br>

5. Connect your PC to your previous Home Wifi System to get internet connectivity. <br>

6. Reconnect to https://unifi.ubnt.com <br>

7. From your Cloud Key Dashboard, go to Devices on the left. You should be able to see the new AP. <br>
<img src="images/07-01-new-ap.png" width="500"> <br>

8. Click the AP icon. <br>

9. Then click ‘Adopt and Upgrade’. <br>
<img src="images/07-02-adopt-ap.png" width="750"> <br>

10. Click ‘Confirm’. <br>

11. Next, you will see the ‘Adopting (Update Required)’ status. <br>

12. When completed, you will see the ‘Connected’ status. <br>
<img src="images/07-03-ap-connected.png" width="300"> <br>

## 8. Setup Your NetGear DOCSIS Modem from Router Mode to Bridge Mode
In this example, I am using a NetGear DOCSIS modem as my cable modem. <br>
1. Connect the NetGear DOCSIS Modem LAN port to your PC. <br>

2. From a web browser, browse your NetGear DOCSIS Modem’s web portal. <br>

3. From the dashboard, go to Advanced > Administration > Router Mode. <br>

4. In the Router mode option, select ‘No’, then click ‘Apply’. <br>
<img src="images/08-01-netgear-router-mode.png" width="500"> <br>

5. you will get the *‘Rebooting the device now, please wait…’* Status. <br>
<img src="images/08-02-netgear-reboot.png" width="500"> <br>

6. After that, you will lose connectivity. <br>

7. Reconnect to the Unifi AP, then from a web browser, browse https://192.168.2.5 (your Cloud Key), then click ‘Manage’. <br>

8. Login using your credential. <br>

9. (Optional) From the Cloud Key dashboard, go to Devices > USG, then right beside the ‘Connected’ status, click the 3 dot icon, then select ‘Restart’. <br>
<img src="images/08-03-usg-restart.png" width="700"> <br>
Then click ‘Confirm’. <br>

10. From the Cloud Key dashboard, go to Devices > USG > Details > WAN 1. <br>
If the IP address is 0.0.0.0 (the WAN port didn’t get DHCP from the provider), then you need to call your ISP to help reset the connection (you can simply say currently there’s no connection). This will help to clear the ISP PE’s ARP and MAC address. <br>

11. After the ISP has reset the connection (typically by sending a reset signal to the modem), then you should be able to get the WAN IP. <br>

## 9. Manage Your Cloud Key Controller Using Your Mobile App
1. From Apple Store / Google Play, search for ‘UniFi Network Mobile app’. <br>

## 10. Setup IPv6 in Your Home Network
You can skip sections 10 - 13 if you don’t want to enable IPv6. <br>
1. Using a web browser, browse to the Cloud Key GUI, then go to Settings > Try New Settings (Beta) > Internet > WAN Networks, then Edit the WAN1 port. <br>

2. In the IPv6 section, <br>
   Connection Type: select ‘Using DHCPv6’ <br>
   Prefix Delegation Size: 60 <br>
   Then click ‘Save’ <br>
   <img src="images/10-01-usg-wan1.png" width="700"> <br>

3. Then go to Networks > Local Networks, then Edit the LAN1 port. <br>

4. In the ‘Configure IPv6 Network’ section,  <br>
   IPv6 Interface Type: select ‘Prefix Delegation’ <br>
   IPv6 Prefix ID: 1 <br>
   IPv6 Prefix Delegation Interface: WAN <br>
   IPv6 RA: tick (v) Enable IPv6 Router Advertisement <br>
   IPv6 RA Priority: select ‘High’ <br>
   You can leave the other settings to default. <br>
   <img src="images/10-02-wan1-ipv6.png" width="700"> <br>

5. Connect your mobile phone to the Unifi AP’s Wireless SSID. <br>

6. SSH to the USG, the do the following commands: <br>
   a. **show ipv6 route** <br>
   <img src="images/10-03-a-sh-ipv6-route.png" width="500"> <br>

   b. **show interfaces** <br>
   <img src="images/10-03-b-sh-interfaces.png" width="500"> <br>

   c. **show ipv6 neighbors | match REACHABLE** <br>
   <img src="images/10-03-c-sh-ipv6-nei.png" width="500"> <br>

   d. **ping6 google.com** <br>
   <img src="images/10-03-d-ping6-google.png" width="500"> <br>

7. If possible, connect a non-SLB laptop to the wireless network, and do ipconfig and tracert -6 facebook.com <br>
<img src="images/10-04-ipconfig.png" width="500"> <br>
<img src="images/10-04-tracert-fb.png" width="700"> <br>

8. If possible, install a traceroute application in your mobile phone, e.g. Hurricane Electric Network Tools, and perform traceroute ipv6 to google.com. <br>
<img src="images/10-05-mobile-tracert-goog.png" width="250"> <br>

## 11. Setup 4 More SSIDs
The other SSIDs we are going to setup is below: <br>
  (previously we have setup Irawan 5: dual stack IPv4+IPv6 dhcp-pd - LAN - Prefix: 1) <br>
  - Irawan 6: ipv6 dhcp-pd - vlan 20 - eth1.20 - LAN1-IPv6-DHCPPD - Prefix: 2 <br>
  - Irawan 66: ipv6 nptv6 - vlan 30 - eth1.30 - LAN1-IPv6-NPTv6 - Prefix: 3 <br>
  - Fransiskus 4: ipv4 - vlan 110 (guest) - eth1.110 - LAN1-Guest-IPv4 <br>
  - Budiman 5: ipv4 + ipv6 dhcp-pd – vlan 210 (dmz) – eth1.210 – LAN1-DMZ – Prefix: 5 <br>

### 11.1 Backup Your Current Controller & USG Settings
1. Using a web browser, browse to the Cloud Key GUI, then go to Settings > Controller Settings > Backup, then go to the Backup/Restore Section > Download Backup, select ‘Settings Only’, then click the ‘Download’ button. <br>
<img src="images/11-01-controller-backup.png" width="700"> <br>

2. SSH to the USG, the save the output of the following commands: <br>
**show configuration commands | no-more** <br>

### 11.2 The Steps To Create A New SSID Associated With An IPv6 Network Only Using DHCPv6-PD
1. Using a web browser, browse to the Cloud Key GUI, then go to Settings > Networks > Local Networks, then click the ‘Create New Local Network’ button. <br>

2. Select the ‘Create Advanced Network’ button. <br>
<img src="images/11-02-a-create-advanced-network.png" width="500"> <br>

3. In the ‘New Advanced Network’ section, <br>
Network Name: LAN1-IPv6-DHCPPD <br>
Network Purpose: Corporate <br>
Network Group: LAN <br>
VLAN ID: 20 <br>
Network Size: Custom <br>
Gateway IP / Subnet: 127.0.0.1/24 (this is intentional) <br>
DHCP Range: 127.0.0.2 - 127.0.0.254 (this is intentional) <br>
IPv6 Interface Type: Prefix Delegation <br>
IPv6 Prefix ID: 2 <br>
IPv6 Prefix Delegation Interface: WAN <br>
IPv6 RA: enabled <br>
IPv6 RA Priority: High <br>
Click the ‘Done’ button <br>
<img src="images/11-02-b-create-irawan6.png" width="700"> <br>

4. Go to Settings > Wi-Fi > Wi-Fi Networks, then click the ‘Create New Wi-Fi Network’ button. <br>

5. Select the ‘Create Advanced Wi-Fi’ button <br>
<img src="images/11-02-c-create-advanced-wifi.png" width="500"> <br>

6. In the ‘Advanced Wi-Fi Creation’ section, <br>
Wi-Fi Name: Irawan 6 <br>
Enable this network: Yes <br>
Security Protocol: WPA Personal <br>
WPA Mode: WPA2 Only <br>
CCMP Encryption: Yes <br>
Enter your WiFi password <br>
Guest Policies: No <br>
Refresh Shared Secret: Yes <br>
Use a VLAN: Yes <br>
VLAN ID: 20 <br>
Combine Name/SSID: Yes <br>
Click the ‘Done’ button <br>

7. For verification, connect your mobile phone to the new SSID. <br>

8. SSH to the USG, the do the following commands: <br>
   a. **show ipv6 route** <br>
   <img src="images/11-02-d-sh-ipv6-route.png" width="500"> <br>

   b. **show interfaces** <br>
   <img src="images/11-02-e-sh-interfaces.png" width="500"> <br>

   c. Using the HE network tools in your mobile phone, traceroute ipv6 to google.com, then do this command in the USG: **show ipv6 neighbors | match REACHABLE**. <br>
   <img src="images/11-02-f-sh-ipv6-neighbors.png" width="500"> <br>

9. Backup your configuration. <br>

### 11.3 The Steps To Create A New SSID Associated With An IPv6 Network Only Using NPTv6
1. Using a web browser, browse to the Cloud Key GUI, then go to Settings > Networks > Local Networks, then click the ‘Create New Local Network’ button. <br>

2. Select the ‘Create Advanced Network’ button. <br>
<img src="images/11-02-a-create-advanced-network.png" width="500"> <br>

3. In the ‘New Advanced Network’ section, <br>
   Network Name: LAN1-IPv6-NPTv6 <br>
   Network Purpose: Corporate <br>
   Network Group: LAN <br>
   VLAN ID: 30 <br>
   Network Size: Custom <br>
   Gateway IP / Subnet: 127.0.1.1/24 (this is intentional) <br>
   DHCP Range: 127.0.1.2 - 127.0.1.254 (this is intentional) <br>
   IPv6 Interface Type: Static <br>
   IPv6 Gateway / Subnet: fd00:0:0:1::1/64 <br>
   IPv6 RA: Enable <br>
   IPv6 RA Priority: select ‘High’ <br>
   DHCPv6: No <br>
   You can leave the other settings to default. <br>
   Click the ‘Done’ button <br>
   <img src="images/11-03-a-create-irawan66.png" width="700"> <br>

4. Go to Settings > Wi-Fi > Wi-Fi Networks, then click the ‘Create New Wi-Fi Network’ button. <br>

5. Select the ‘Create Advanced Wi-Fi’ button. <br>
<img src="images/11-02-c-create-advanced-wifi.png" width="500"> <br>

6. In the ‘Advanced Wi-Fi Creation’ section, <br>
   Wi-Fi Name: Irawan 66 <br>
   Enable this network: Yes <br>
   Security Protocol: WPA Personal <br>
   WPA Mode: WPA2 Only <br>
   CCMP Encryption: Yes <br>
   Enter your WiFi password <br>
   Guest Policies: No <br>
   Refresh Shared Secret: Yes <br>
   Use a VLAN: Yes <br>
   VLAN ID: 30 <br>
   Combine Name/SSID: Yes <br>
   Click the ‘Done’ button <br>

7. Backup your configuration <br>

8. SSH to the USG, the do the following commands <br>
   sudo ip6tables -L <br>
   sudo ip6tables -t nat -L <br>
   sudo ip6tables -t nat -L -n -v <br>

   sudo ip6tables -t nat -A POSTROUTING -o eth0 -s fd00:0:0:1::/64 -j NETMAP --to 2601:2c3:8580:f1b3::/64 <br>
   sudo ip6tables -t raw -D OUTPUT -j NOTRACK <br>
   sudo ip6tables -t raw -D PREROUTING -j NOTRACK <br>

   sudo ip6tables -L <br>
   sudo ip6tables -t nat -L <br>
   sudo ip6tables -t nat -L -n -v <br>

9. For verification, connect your mobile phone to the new SSID. <br>

10. SSH to the USG, the do the following commands: <br>
   a. **show ipv6 route** <br>
   <img src="images/11-03-b-sh-ipv6-route.png" width="500"> <br>
   
   b. **show interfaces** <br>
   <img src="images/11-03-c-sh-interfaces.png" width="500"> <br>
   
   c. Using the HE network tools in your mobile phone, traceroute ipv6 to google.com, then do this CLI command in the USG: **sudo ip6tables -t nat -L**. <br>
   <img src="images/11-03-d-mobile-tracert-goog.png" width="300"> <br>
   <img src="images/11-03-e-iptables.png" width="600"> <br>
   
   d. **sudo ip6tables -t nat -L -n -v** <br>
   <img src="images/11-03-f-iptables2.png" width="750"> <br>

11. Backup your configuration. <br>

### 11.4 The Steps To Create A New Guest SSID Associated With IPv4 Subnet Only
At the moment of this writing, Guest IPv6 deployment is currently not supported by Ubiquiti, due to the lack of IPv6 Firewall Rules around the Guest IPv6 setup. Check the article [here](https://community.ui.com/questions/Bug-Unifi-Doesnt-Pass-IPv6-RAs-to-Guest-Tagged-Networks/4673e39c-2235-4470-9bcb-e4b45d64a5a9). <br>

1. Using a web browser, browse to the Cloud Key GUI, then go to Settings > Networks > Local Networks, then click the ‘Create New Local Network’ button. <br>

2. Select the ‘Create Advanced Network’ button. <br>
<img src="images/11-02-a-create-advanced-network.png" width="500"> <br>

3. In the ‘New Advanced Network’ section, <br>
   Network Name: LAN1-Guest-IPv4 <br>
   Network Purpose: Guest <br>
   Network Group: LAN <br>
   VLAN ID: 110 <br>
   Network Size: Custom <br>
   Gateway IP / Subnet: 192.168.4.1/24 <br>
   DHCP Range: 192.168.4.6 - 192.168.4.254 <br>
   IPv6 Interface Type: None <br>
   You can leave the other settings to default. <br>
   Click the ‘Done’ button <br>

4. Go to Settings > Hotspot > General, then click the ‘Enable Hotspot’ radio button <br>
   Authentication Mode: Hotspot <br>
   Enable Terms of Service: Yes <br>
   Enable Welcome Text: No <br>
   Click the ‘Apply Changes’ button <br>

5. Go to Settings > Hotspot > Vouchers, then click the ‘Enable Voucher-based Authorization’ radio button <br>
   Click the ‘Apply Changes’ button <br>

6. Go to Settings > Hotspot > Advanced <br>
   Session Expiration: 24 hours <br>
   Enable HTTPS Redirection: Yes <br>
   Enable encrypted redirect URL: Yes <br>
   Leave the other settings as they are. <br>

7. Go to Settings > Wi-Fi > Wi-Fi Networks, then click the ‘Create New Wi-Fi Network’ button <br>

8. Select the ‘Create Advanced Wi-Fi’ button. <br>
<img src="images/11-02-c-create-advanced-wifi.png" width="500"> <br>

9. In the ‘Advanced Wi-Fi Creation’ section, <br>
   Wi-Fi Name: Fransiskus 4 <br>
   Enable this network: Yes <br>
   Security Protocol: WPA Personal <br>
   WPA Mode: WPA2 Only <br>
   CCMP Encryption: Yes <br>
   Enter your WiFi password <br>
   Guest Policies: Yes <br>
   Refresh Shared Secret: Yes <br>
   Use a VLAN: Yes <br>
   VLAN ID: 110 <br>
   Combine Name/SSID: Yes <br>
   Click the ‘Done’ button <br>
   (Note: after this operation, your wireless connection will be down for approx. 1 minute) <br>

10. Using a web browser, browse to https://192.168.2.5:8443/manage/hotspot/account/login/ <br>
   Click ‘Vouchers’ on the left, then select the ‘Create Vouchers’ button. <br>
   Create 1 voucher <br>
   Quota: Multi use (unlimited) <br>
   Expiration Time: 24 hours <br>
   Click the ‘Save’ button <br>
   Write down the voucher code. <br>
   <img src="images/11-04-a-voucher.png" width="800"> <br>

11. For verification, connect your mobile phone to the new SSID. <br>

12. SSH to the USG, the do the following commands: <br>
   a. **show ipv6 route** <br>
   <img src="images/11-04-b-sh-ipv6-route.png" width="500"> <br>

   b. **show interfaces** <br>
   <img src="images/11-04-c-sh-interfaces.png" width="500"> <br>

13. Backup your configuration. <br>

## 12. Setup The Firewall Policy to Secure Your Deployment
This step has been taken care of by the USG’s default firewall rule. <br>

## 13. Restart Your USG To Ensure The Setting Is Permanent, Even After Power Failure
Note: I did power off my Cable Modem, USG, Cloud Key, and AP. <br>
After power on, I noticed I still got the same DHCP IP from Comcast. <br>
<img src="images/13-a-show-interfaces.png" width="500"> <br>

But I lost the NPTv6 setting. <br>
<img src="images/13-b-ip6tables.png" width="400"> <br>

To summarize, <br>
 &nbsp;- Irawan 5: dual stack IPv4+IPv6 dhcp-pd **worked** after restart. <br>
 &nbsp;- Irawan 6: ipv6 dhcp-pd **worked** after restart. <br>
 &nbsp;- Irawan 66: ipv6 nptv6 **didn’t work** after restart. <br>
 &nbsp;- Fransiskus 4: Guest ipv4 **worked** after restart. <br>

Note, Irawan 66 ipv6 nptv6 worked after I added the below 3 commands: <br>
**sudo ip6tables -t nat -A POSTROUTING -o eth0 -s fd00:0:0:1::/64 -j NETMAP --to 2601:2c3:8580:f1b3::/64** <br>
**sudo ip6tables -t raw -D OUTPUT -j NOTRACK** <br>
**sudo ip6tables -t raw -D PREROUTING -j NOTRACK** <br>

I guess the 3 above commands are not permanent. <br>

## 14. How To Capture Your Specific Wireless Device Traffic Using The USG
1. Gather your wireless device’s MAC address. E.g. d0:81:7a:7f:52:3c <br>

2. SSH into your USG <br>

3. do the below command to capture the wireless device’s traffic <br>
**sudo tcpdump -npi eth1 'ether dst d0:81:7a:7f:52:3c or ether src d0:81:7a:7f:52:3c' -w /home/yordan12/cap20191229iphone01.pcapng** <br>

4. once completed, press **Ctrl+C** <br>

5. Use WinSCP to copy the file to your PC <br>

## 15. Setup Pi-Hole in Raspberry Pi to Get A Secure Home DNS Server
For this project, I am using the Raspberry 3B+ with the latest Raspbian OS. <br>
Note: Raspberry 3B+ was selected because it has a wireless interface. <br>
Below are the steps: <br>
1. Write an image to the SD card <br>
   - download & install **etcher** - https://www.balena.io/etcher/ <br>
   - insert SD card into laptop <br>
   - select zip / img file (2019-09-26-raspbian-buster.img) <br>
   - verify SD card is detected <br>
   - click 'Flash!' (this requires elevated admin rights) <br>
   - wait for 15-20 minutes, until you see the successful note. <br>
   - remove the SD card from laptop and connect it back to the Raspberry Pi. <br>

2. Connect the Raspberry Pi to a keyboard, mouse (using USB), and a screen (using HDMI) for the initial setup. <br>

3. Update the Raspberry Pi Software (this process might take 30 minutes) <br>
   **sudo apt-get update** <br>
   **sudo apt-get upgrade** <br>

4. Enable SSH Server <br>
   - **sudo raspi-config** <br>
     - select Interfacing Options <br>
     - select SSH <br>
     - enable SSH <br>
     - select Finish <br>
   - check more at https://www.raspberrypi.org/documentation/remote-access/ssh/README.md <br>

5. Install VNC Server (optional). SSH into your Raspberry Pi and do below commands. <br>
   - **sudo apt-get install tightvncserver** <br>
   - **sudo tightvncserver** <br>
     Setup a password (max 8 digit) <br>
   - **sudo vncserver -kill :1** <br>
   - **sudo vncserver :1 -geometry 800x600 -depth 24** <br>
   - reboot RPi <br>
   - **sudo netstat -plutn** <br>
   - **sudo tightvncserver** <br>
   - **sudo netstat -plutn** <br>
   (to connect from win10, open tightvncviewer, then connect to **<rpi IP>:5901**) <br>
   
6. Secure your Raspberry Pi. SSH into your Raspberry Pi and do below commands. <br>
     1. Change your default password <br>
       - **sudo raspi-config** <br>
       - select **Change User Password** <br>
     2. Change your username (change yordan12 with your preferred username) <br>
       - **sudo adduser yordan12** <br>
       - **sudo usermod -a -G adm,dialout,cdrom,sudo,audio,video,plugdev,games,users,input,netdev,gpio,i2c,spi yordan12** <br>
       - **sudo su - yordan12** <br>
     3. Make sudo requires a password <br>
       - **sudo nano /etc/sudoers.d/010_pi-nopasswd** <br>
         pi ALL=(ALL) PASSWD: ALL <br>
         yordan12 ALL=(ALL) PASSWD: ALL <br>
     4. Ensure you have the latest security fix for your SSH server <br>
       - **sudo apt install openssh-server** <br>
     5. Only allow your username to log in <br>
       - **sudo nano /etc/ssh/sshd_config** <br>
          AllowUsers yordan12 <br>
          **sudo systemctl restart ssh** <br>
     6. Change your Raspberry Pi hostname <br>
         **sudo nano /etc/hostname** <br>
         **sudo nano /etc/hosts** <br>
         **sudo reboot** <br>
   - check more at https://www.raspberrypi.org/documentation/configuration/security.md <br>

### 15.1. Pi-Hole Installation
1. Install Pi-Hole DNS Server and Ad Blocker <br>
   **curl -sSL https://install.pi-hole.net | bash** <br>
   click OK, OK, OK, <br>
   select wlan0 <br>
   select **Quad9 (filtered, DNSSEC)** <br>
   click OK, OK,  <br>
   configure static IP address for your RPI: 192.168.1.2/24 <br>
   leave the default gateway to 192.168.1.1 <br>
   web admin interface: on (recommended) <br>
   web server: on <br>
   log queries: on <br>
   show everything <br>
   click OK <br>

2. Note down the provided ‘Admin Webpage login password’ and restart the Raspberry Pi <br>
   **sudo shutdown -r now** <br>

3. (optional) setup a different Admin Web Password <br>
   **pihole -a -p** <br>
   **pihole -h** (for a list of available commands) <br>

4. Access the Pi-Hole web interface using a web browser: http://192.168.1.2/admin <br>
Login using the password in the previous step <br>
(note, if Chrome shows you index.php, restart your laptop or try to use another browser before reimaging the Raspberry Pi) <br>

5. Reconfigure the USG so that Pi-hole is the DNS Server for your home network <br>
   1. Using a web browser, login to your UniFi Cloud Key <br>
       In the Settings > Configuration > Network Settings, change the parameter below: <br>
       Primary DNS: **192.168.1.2** (Pi-Hole) <br>
       Secondary DNS: 75.75.75.75 (Comcast DNS Server) <br>
       Click Apply Changes <br>
	   <img src="images/15-a-dns-pihole.png" width="500"> <br>

   2. Go to Settings > Networks > Local Networks > LAN > Edit <br>
       Go to DHCP Controls > DHCP Name Server, select Manual <br>
       DNS Server 1: **192.168.1.2** (Pi-Hole) <br>
       DNS Server 2: 75.75.75.75 (Comcast DNS Server) <br>

       Go to DHCPv6/RDNSS DNS Control, select Manual <br>
       In the DHCPv6/RDNSS Name Server: <br>
       DNS Server 1: **2601:2c3:8580:f1b1:ac06:6918:49a2:9b10** (Pi-Hole) <br>
       DNS Server 2: 2001:4860:4860::8888 (Google DNS Server) <br>

    Click Apply Changes <br>

   3. Do the same steps for the other Local Network subnets. <br>

   4. Disconnect and Reconnect your wireless devices, then monitor the Pi-Hole Web Admin Interface http://192.168.1.2/admin <br>
      <img src="images/15-b-pihole-dashboard.png" width="800"> <br>
	  <img src="images/15-c-pihole-query.png" width="800"> <br>
	  <img src="images/15-d-pihole-blocked.png" width="400"> <br>
	  <img src="images/15-e-usg-netstat.png" width="700"> <br>

### 15.2. How to Run Another Webserver While Using Pi-Hole
To do this, I was using this guide: https://rafaelc.org/tech/p/running-a-web-server-while-using-pi-hole/ <br>
And this guide: https://rafaelc.org/tech/p/web-server-and-pi-hole-a-how-to/ <br>
In summary, below were the steps I took: <br>

#### 1. Step 1 - Set A Secondary Static IP
In this first step, we will configure your Pi to have two static IP addresses, one for Pi Hole and other to be used by your web apps. <br>

##### 1.1. Primary Static IP
Let’s make sure **dhcpcd** is instructed to use a static IP. <br>
Your **/etc/dhcpcd.conf** should have something similar to this: <br>
```
# /etc/dhcpcd.conf
interface wlan0  
static ip_address=192.168.1.2/24  
static routers=192.168.1.1  
static domain_name_servers=127.0.0.1  
```

We also set the DNS for 127.0.0.1, so the Pi uses the Pi Hole to resolve the names he needs. <br>
The gateway and the netmask should be set accordingly to your network. <br>
 
Restart dhcpcd or reboot your system: <br>
```
$ sudo systemctl daemon-reload  
$ sudo systemctl restart networking  
$ sudo systemctl restart dhcpcd  
```

Issue **ip a** and check if the Pi got your chosen IP. <br>

##### 1.2. Secondary Static IP
Now let’s set the secondary IP. <br>
The approach is to bring up the second IP through **/etc/network/interfaces**. <br>

Place a file under **/etc/network/interfaces.d**, and name it for example secondary_ip. Add the following lines, replacing the values with the appropriate ones: <br>
```
$ sudo nano /etc/network/interfaces.d/secondary_ip  
# /etc/network/interfaces.d/secondary_ip  
auto wlan0  
    iface wlan0 inet static  
    address 192.168.1.3  
    netmask 255.255.255.0  
    gateway 192.168.1.1  
    dns-nameservers 127.0.0.1  
```

Have you done the proper configuration, restart your network or reboot. Your Pi should now have two static IPs. Let’s check it. <br>
```
$ sudo systemctl reboot
$ ip a
```

If all went well, you will see both IPs in two lines starting with inet. For our example: <br>
```
   ...  
   inet 192.168.1.3/24 brd 192.168.1.255 scope global wlan0  
     valid_lft forever preferred_lft forever  
   inet 192.168.1.2/24 brd 192.168.1.255 scope global secondary noprefixroute wlan0  
     valid_lft forever preferred_lft forever  
   ...  
```

#### 2. Step 2 - Bind Lighttpd To The Primary IP
Now we are going to make Lighttpd bind only to the primary address. This will leave port 80 free for our webserver in the other address. This step is much more straight-forward. <br>
Now, the Lighttpd config done by Pi Hole sources a separate file, allowing us to permanently store our modifications in it. <br>
This file is **/etc/lighttpd/external.conf**. Add this line to it, replacing 192.168.1.2 by the primary IP address you just set: <br>
**server.bind = "192.168.1.2"** <br>

Let’s restart Lighttpd: <br>
```
sudo systemctl restart lighttpd
```

Now the Pi Hole admin console should only be available on your primary IP. <br>

#### 3. Step 3 - Bind Your Web Server To The Secondary IP
Finally, we need to make your web server bind only to the secondary IP. How to do this, obviously, depends on which web server you pick and your specific configuration. <br>
However, the procedure is analogous to what we did on step 2. Edit the configuration of the chosen server and bind it to your Pi’s secondary IP. <br>
For example, on Apache2, you do it using the listen directive. <br>
```
$ sudo nano /etc/apache2/ports.conf  
     Listen 192.168.1.3:80  
   
   <IfModule ssl_module>  
     Listen 192.168.1.3:443  
   </IfModule>  

   <IfModule mod_gnutls.c>  
     Listen 192.168.1.3:443  
   </IfModule>  
```

After making the needed modifications, start the web server and everything should work as we wanted. <br>
```
$ sudo apache2ctl configtest
$ sudo systemctl restart apache2
```

Alternatively, if Pi Hole is already installed, reconfigure it to make sure it points to that address. Run **pihole -r** and select **Reconfigure**. <br>
This step is necessary because it tells Pi Hole to resolve the blocked domains to that address. It expects Lighttpd to be there to serve the **Pi Hole blocked** page. <br>
Also notice that this guide placed Pi Hole and Lighttpd in the primary IP and your webserver in the secondary, but the opposite is obviously also possible. The key is that Pi Hole and Lighttpd should be configured to the same address. <br>
As a side note, the DNS server binds to both IPs and we will not touch it. We are only concerned about port 80, so this fact doesn’t bring any issues and is irrelevant for our goals. <br>
Done. You should now be able to run Pi Hole and a webserver simultaneously and without issues on your Pi. <br>

## 16. Setup Cacti in The Raspberry Pi to Get A Network Monitoring Server
1. Install Cacti <br>
**sudo apt-get install cacti** <br>
- You will be asked to select the web server for which Cacti should be automatically configured. Choose apache2 <br>
- You will also be asked to configure database for cacti with dbconfig-common. Press <Yes> <br>
- You will then be asked to configure the MySQL root user: <type your password> <br>
(Cacti will take a while to install because of the additional packages to be downloaded and installed.) <br>

2. Login to Cacti <br>
- Open a web browser and point it to http://192.168.5.2/cacti <br>
- Enter username: admin and password: <your Mysql root password>, and press Login. <br>
Success! <br>

3. Setup SNMP RO on the home devices <br>
To setup SNMP on raspberry pi: https://bigdanzblog.wordpress.com/2015/01/03/installing-snmp-onto-a-raspberrypi/ <br>

To install it: <br>
```
sudo apt-get install snmpd
sudo apt-get install snmp
```
To get it running, you will need to modify the **/etc/snmp/snmpd.conf** file: <br>
First, I commented out this line: <br>
```
#agentAddress udp:127.0.0.1:161
```
And below the line **#agentAddress udp:161,udp6:[::1]:161** I added: <br>
```
agentAddress udp:161
```
Then below this line **rocommunity public localhost** I added: <br>
```
rocommunity public 192.168.5.2/32
```
Finally, restart the service:  <br>
```
sudo service snmpd restart
```
To verify the service has started: <br>
```
ps -A | grep snmpd
```
If the service did not start, you can check **/var/log/syslog** for error messages. <br>
You can also run tcpdump to monitor SNMP packets on the RPI to verify they are being received and respond to: <br>
```
tcpdump -i eth0 “udp and (src port 161 or 162)”
```
On the RPI, you can walk the MIB using this command (Unfortunately, no MIBs appear to be installed to properly interpret the OIDs so you mainly get gibberish, but it is a good test to verify snmpd itself is working.): <br>
```
snmpwalk -Os -c public -v 1 localhost
```
To see information about RPI disks: <br>
```
snmpwalk -Os -c public -v 1 nsdude UCD-SNMP-MIB::dskTablecd /
```
Information about CPU usage (load average): <br>
```
snmpwalk -Os -c public -v 1 nsdude UCD-SNMP-MIB::laTable
```

4. To setup SNMP on the UniFi devices: Settings > Gateway > SNMP > Enable SNMP V1 & V2C, click Apply Changes <br>

5. Using Cacti (https://cacti.net/downloads/docs/pdf/manual.pdf) <br>
   - Open a web browser and point it to http://192.168.5.2/cacti <br>
   - Click Create devices. <br>
   - You can see in the above screenshot that Localhost, which refers to the Pi, is already listed under Devices. <br>
   - Click Add at the top right of the page, and press Create at the bottom right of the page after filling details. <br>
   - You should notice that Cacti will fetch details of the Cisco router. <br>
   - Click Create Graphs for this Host. <br>
   - Select the checkbox next to eth0 and press Create. <br>
   - Go to Management > Graph Management in the left pane. <br>
   - Click My Cisco Router – Traffic – eth0 to see the graph generated by Cacti. <br>
   - Please note that it may take a few minutes for the graph to be generated. <br>
   - If you don’t see a graph initially, just hang on and the graph will appear. <br>

## 17. Setup A Static Website In Another Raspberry Pi
I want the below services to be available from the internet: <br>
http to my apache-pi using ipv6: http://ipv6.yordan12.com <br>
http to my apache-pi using ipv4: http://ipv4.yordan12.com <br>

To achieve this, I purchased a domain from https://domains.google.com/ - it cost me US$ 12 / year. <br>
Backup the configuration first. <br>

### 17.1. Create A New Local Network & SSID Associated With an IPv4 & IPv6 Network Using DHCPv6-PD
1. Using a web browser, browse to the Cloud Key GUI, then go to Settings > Networks > Local Networks, then click the ‘Create New Local Network’ button. <br>

2. Select the ‘Create Advanced Network’ button. <br>
<img src="images/11-02-a-create-advanced-network.png" width="500"> <br>

3. In the ‘New Advanced Network’ section, <br>
   Network Name: LAN1-DMZ <br>
   Network Purpose: Corporate <br>
   Network Group: LAN <br>
   VLAN ID: 210 <br>
   Network Size: Custom <br>
   Gateway IP / Subnet: 192.168.5.1/24 <br>
   DHCP Range: 192.168.5.6 – 192.168.5.254 <br>
     Go to DHCP Controls > DHCP Name Server, select Manual <br>
       DNS Server 1: 192.168.1.2 (Pi-Hole) <br>
       DNS Server 2: 75.75.75.75 (Comcast DNS Server) <br>

   IPv6 Interface Type: Prefix Delegation <br>
   IPv6 Prefix ID: 5 <br>
   IPv6 Prefix Delegation Interface: WAN <br>
   IPv6 RA: enabled <br>
   IPv6 RA Priority: High <br>
    
     Go to DHCPv6/RDNSS DNS Control, select Manual <br>
       In the DHCPv6/RDNSS Name Server: <br>
       DNS Server 1: 2601:2c3:8580:f1b1:ac06:6918:49a2:9b10 (Pi-Hole) <br>
       DNS Server 2: 2001:4860:4860::8888 (Google DNS Server) <br>
    
   Click the ‘Done’ button. <br>
   <img src="images/17-a-network-created.png" width="700"> <br>

4. Go to Settings > Wi-Fi > Wi-Fi Networks, then edit the ‘Irawan 66’ SSID. <br>

5. In the ‘Edit Wi-Fi Network’ section, <br>
   Wi-Fi Name: Budiman 5 <br>
   Enable this network: Yes <br>
   Security Protocol: WPA Personal <br>
   WPA Mode: WPA2 Only <br>
   CCMP Encryption: Yes <br>
   Enter your WiFi password <br>
   Guest Policies: No <br>
   Hide SSID: No <br>
   Refresh Shared Secret: Yes <br>
   Use a VLAN: Yes <br>
   VLAN ID: 210 <br>
   Combine Name/SSID: Yes <br>
   Click the ‘Apply Changes’ button <br>
   (you will lose wireless connectivity for 1 minute) <br>

7. For verification, connect your mobile phone to the above SSID. <br>

8. SSH to the USG, the do the following commands: <br>
   a. **show ipv6 route** <br>
   <img src="images/17-b-sh-ipv6-route.png" width="500"> <br>

   b. **show interfaces** <br>
   <img src="images/17-c-sh-interfaces.png" width="500"> <br>

   c. Using the HE network tools in your mobile phone, traceroute ipv6 to google.com, then do this CLI command in the USG: **show ipv6 neighbors | match REACHABLE**. <br>
   <img src="images/17-d-sh-ipv6-neighbors.png" width="500"> <br>

9. Backup your configuration. <br>

10. Connect this new Raspberry-pi (apache-pi) to the new SSID. <br>

### 17.2. Setup The Testing Web Page, Using the Budiman 5 SSID
1. Do the exact same steps 1 to 6 in the [Section 15](#15-setup-pi-hole-in-raspberry-pi-to-get-a-secure-home-dns-server) above. <br>

#### 2. Edit dhcpcd.conf
```
$ sudo nano /etc/dhcpcd.conf
interface wlan0
        static ip_address=192.168.5.2/24
        static routers=192.168.5.1
        static domain_name_servers=192.168.1.2 192.168.1.1

```
For more information, see https://www.raspberrypi.org/forums/viewtopic.php?t=245701. <br>

#### 3. Reboot The Raspberry Pi, To Bring The IP Configuration Into Effect
```
sudo reboot
```

#### 4. Setup The Testing Page
For more information, see https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-debian-9. <br>

Check apache2 status: <br>
```
sudo systemctl status apache2
```

To get the system’s IP: <br>
```
hostname -I
```
Do the below steps: <br>
```
$ sudo mkdir -p /var/www/yordan12.com/html
$ sudo chown -R $USER:$USER /var/www/yordan12.com/html
$ sudo chmod -R 755 /var/www/yordan12.com
$ sudo nano /var/www/yordan12.com/html/index.html
$ sudo nano /etc/apache2/sites-available/yordan12.com.conf
<VirtualHost *:80>
    ServerAdmin admin@yordan12.com
    ServerName yordan12.com
    ServerAlias www.yordan12.com
    DocumentRoot /var/www/yordan12.com/html
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    Header always set X-Content-Type-Options "nosniff"
    Header always set X-Frame-Options "SAMEORIGIN"
    Header always set X-XSS-Protection "1; mode=block"
    Header always set Cache-Control: no-store
    Header always set Set-Cookie: "Max-Age=0; path=/; domain=.yordan12.com; HttpOnly; Secure; SameSite=Strict;"
    Header always set Content-Security-Policy: "default-src 'self' ; script-src 'self' ; style-src 'self' ; img-src 'self' ; font-src 'self' ; connect-src 'self' ; media-src 'self' ; object-src 'self' ; child-src 'self' ; frame-src'self' ; worker-src 'self' ; frame-ancestors 'self' ; form-action 'self' ; manifest-src 'self' ;"
    Header always set Referrer-Policy: no-referrer
    Header always set Feature-Policy: "vibrate 'self'; sync-xhr 'self';"
</VirtualHost>

$ sudo nano /etc/apache2/apache2.conf
ServerTokens Prod
ServerSignature Off
ServerName localhost

$ sudo a2ensite yordan12.com.conf
$ sudo a2dissite 000-default.conf
$ sudo a2ensite yordan12.com-le-ssl.conf
$ sudo a2dissite default-ssl.conf
$ sudo a2enmod headers

$ sudo apache2ctl configtest
$ sudo systemctl restart apache2
```

#### 5. How To Make Your Apache Server More Secure
5.1. add a non-admin user <br>
```
$ sudo adduser budiman1
```

Note: you can always just add the user and remove them from the admin group. The groups are listed in **/etc/group**. <br>
Just look for the admin line and remove the user names that you dont want. <br>

5.2. Run as an unprivileged user <br>
Edit the file and change the following lines: <br>
```
sudo nano /etc/apache2/envvars

# Since there is no sane way to get the parsed apache2 config in scripts, some
# settings are defined via environment variables and then used in apache2ctl,
# /etc/init.d/apache2, /etc/logrotate.d/apache2, etc.
export APACHE_RUN_USER=budiman1
export APACHE_RUN_GROUP=budiman1
```

5.3. Disable ServerTokens <br>
```
sudo nano /etc/apache2/conf-available/security.conf
```

In that file you will see several **ServerTokens**. Make sure they are all commented out and only **ProductOnly** appears. <br>
```
# ServerTokens 
#ServerTokens Minimal
#ServerTokens OS
#ServerTokens Full
ServerTokens ProductOnly
```

Restart Apache to update the changes.
```
sudo apache2ctl configtest
sudo systemctl restart apache2
```

5.4. Enable mod_reqtimeout
```
$ sudo nano /etc/apache2/apache2.conf
RequestReadTimeout header=10-20,MinRate=500 body=20,MinRate=500
```

Restart Apache to update the changes.
```
sudo apache2ctl configtest
sudo systemctl restart apache2
```

6. Using your web browser, open this url: http://192.168.5.2
Success!

## 18. Setup A Remote User VPN Using OpenVPN
I initially tried to configure L2TP Remote Access VPN in the USG using this reference: https://help.ubnt.com/hc/en-us/articles/115005445768-UniFi-USG-Configuring-L2TP-Remote-Access-VPN <br>
However, this made my USG fall into continuous reboot, so I didn’t try using L2TP further. <br>

I then tried to configure OpenVPN in the Raspberry Pi, and it's working. I was using the below references. <br>
https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-18-04 <>
https://spin.atomicobject.com/2019/04/07/openvpn-raspberry-pi/ <>

Below are the steps I used: <br>
### 18.1. Set Up SSH Keys In Both Raspberry Pis
I used the steps mentioned here: https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-1804. <br>
In this step, we will generate an SSH keypair for each server, then add the OpenVPN server’s public SSH key to the CA machine’s authorized_keys file and vice versa. <br>
Note: <br>
my Pi-hole (RPi 1) will become the Certificate Authority Machine, <br>
my apache-pi (RPi 2) will become the OpenVPN Server. <br>

1. SSH to the apache-pi and do below commands <br>
```
ssh-keygen
```
Enter the passphrase: *yourpassphrase* <br>

2. Copy the Public Key to the pi-hole <br>
```
ssh-copy-id yordan12@192.168.1.2
```
Enter **yes**, then provide the pi-hole ssh password. <br>

3. Authenticate to the pi-hole Using SSH Keys. <br>

4. SSH to the pi-hole and do steps 1-3 above towards the apache-pi. <br>


### 18.2. Setup CA Server and OpenVPN Server In 2 Different Pis
#### 1. Step 1 - Installing OpenVPN and EasyRSA
1.1. SSH to the apache-pi and do below commands. <br>
```
sudo apt update
sudo apt install openvpn
```

1.2. To begin building the CA and PKI infrastructure, use wget to download the latest version of EasyRSA on both your CA machine and your OpenVPN server. <br>
SSH to both the apache-pi and the pi-hole and follow the below instructions to install EasyRSA. <br>
To get the latest version, go to the Releases page on the official EasyRSA GitHub project, copy the download link for the file ending in .tgz, and then paste it into the following command: <br>
```
 wget -P ~/ https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.6/EasyRSA-unix-v3.0.6.tgz
 ```

1.3. Then extract the tarball: <br>
```
cd ~
tar xvf EasyRSA-unix-v3.0.6.tgz
```

#### 2. Step 2 - Configuring the EasyRSA Variables and Building the CA
2.1. SSH to the pi-hole (CA machine) and do the below commands: <br>
```
cd ~/EasyRSA-v3.0.6/
cp vars.example vars
sudo nano vars
```

2.2. Find the settings that set field defaults for new certificates. It will look something like this: <br>
```
#set_var EASYRSA_REQ_COUNTRY    "US"
#set_var EASYRSA_REQ_PROVINCE   "California"
#set_var EASYRSA_REQ_CITY       "San Francisco"
#set_var EASYRSA_REQ_ORG        "Copyleft Certificate Co"
#set_var EASYRSA_REQ_EMAIL      "me@example.net"
#set_var EASYRSA_REQ_OU         "My Organizational Unit"
```

2.3. Uncomment these lines and update the highlighted values to whatever you’d prefer, but do not leave them blank. <br>
For example: <br>
```
set_var EASYRSA_REQ_COUNTRY     "US"
set_var EASYRSA_REQ_PROVINCE    "Texas"
set_var EASYRSA_REQ_CITY        "Houston"
set_var EASYRSA_REQ_ORG         "yordan12"
set_var EASYRSA_REQ_EMAIL       "yordan12@gmail.com"
set_var EASYRSA_REQ_OU          "Personal"
```
When you are finished, save and close the file. <br>

2.4. Run this script with the init-pki option to initiate the public key infrastructure on the CA server: <br>
```
./easyrsa init-pki
```

2.5. After this, call the easyrsa script again, following it with the build-ca option. <br>
```
./easyrsa build-ca nopass
```

#### 3. Step 3 - Creating the Server Certificate, Key, and Encryption Files
Now that you have a CA ready to go, you can generate a private key and certificate request from your server and then transfer the request over to your CA to be signed, creating the required certificate. <br>

3.1. Start by navigating to the EasyRSA directory on your OpenVPN server (apache-pi): <br>
```
cd EasyRSA-v3.0.6/
```

3.2. From there, run the easyrsa script with the init-pki option. <br>
```
./easyrsa init-pki
```

3.3. Then call the easyrsa script again, this time with the gen-req option followed by a common name for the machine. <br>
Throughout this tutorial, the OpenVPN server’s common name will simply be “server”. Be sure to include the nopass option as well. Failing to do so will password-protect the request file which could lead to permissions issues later on: <br>
```
./easyrsa gen-req server nopass
```

3.4. Copy the server key to the /etc/openvpn/ directory: <br>
```
sudo cp ~/EasyRSA-v3.0.6/pki/private/server.key /etc/openvpn/
```

3.5. Using a secure method (like SCP, in our example below), transfer the server.req file to your CA machine: <br>
```
scp ~/EasyRSA-v3.0.6/pki/reqs/server.req yordan12@192.168.1.2:/tmp
```

3.6. Next, on your CA machine (pi-hole), navigate to the EasyRSA directory: <br>
```
cd EasyRSA-v3.0.6/
```

3.7. Using the easyrsa script again, import the server.req file, following the file path with its common name: <br>
```
./easyrsa import-req /tmp/server.req server
```

3.8. Then sign the request by running the easyrsa script with the sign-req option, followed by the request type and the common name. <br>
The request type can either be client or server, so for the OpenVPN server’s certificate request, be sure to use the server request type: <br>
```
./easyrsa sign-req server server
```

3.9. Next, transfer the signed certificate back to your VPN server using a secure method: <br>
```
scp pki/issued/server.crt yordan12@192.168.5.2:/tmp
```

3.10. Before logging out of your CA machine, transfer the ca.crt file to your server as well: <br>
```
scp pki/ca.crt yordan12@192.168.5.2:/tmp
```

3.11. Next, log back into your OpenVPN server (apache-pi) and copy the server.crt and ca.crt files into your /etc/openvpn/ directory: <br>
```
sudo cp /tmp/{server.crt,ca.crt} /etc/openvpn/
```

3.12. Then navigate to your EasyRSA directory: <br>
```
cd EasyRSA-v3.0.6/
```

3.13. From there, create a strong Diffie-Hellman key to use during key exchange by typing: <br>
```
./easyrsa gen-dh
```
<img src="images/18-a-dh.png" width="700"> <br>

3.14. Once it does, generate an HMAC signature to strengthen the server’s TLS integrity verification capabilities: <br>
```
openvpn --genkey --secret ta.key
```

3.15. When the command finishes, copy the two new files to your /etc/openvpn/ directory: <br>
```
sudo cp ~/EasyRSA-v3.0.6/ta.key /etc/openvpn/
sudo cp ~/EasyRSA-v3.0.6/pki/dh.pem /etc/openvpn/
```

With that, all the certificate and key files needed by your server have been generated. <br>
you are ready to create the corresponding certificates and keys which your client machine will use to access your OpenVPN server. <br>

#### 4. Step 4 - Generating a Client Certificate and Key Pair
We will generate a single client key and certificate pair for this guide. If you have more than one client, you can repeat this process for each one. Please note, though, that you will need to pass a unique name value to the script for every client. Throughout this tutorial, the first certificate/key pair is referred to as vpnclient1. <br>

4.1. Get started by creating a directory structure within your home directory to store the client certificate and key files: <br>
```
mkdir -p ~/client-configs/keys
```

4.2. Since you will store your clients’ certificate/key pairs and configuration files in this directory, you should lock down its permissions now as a security measure: <br>
```
chmod -R 700 ~/client-configs
```

4.3. Next, navigate back to the EasyRSA directory and run the easyrsa script with the **gen-req** and **nopass** options, along with the common name for the client: <br>
```
cd ~/EasyRSA-v3.0.6/
./easyrsa gen-req vpnclient1 nopass
```

4.4. Press ENTER to confirm the common name.  <br>
Then, copy the vpnclient1.key file to the /client-configs/keys/ directory you created earlier: <br>
```
cp pki/private/vpnclient1.key ~/client-configs/keys/
```

4.5. Next, transfer the vpnclient1.req file to your CA machine using a secure method: <br>
```
scp pki/reqs/vpnclient1.req yordan12@192.168.1.2:/tmp
```

4.6. Log in to your CA machine (pi-hole), navigate to the EasyRSA directory, and import the certificate request: <br>
```
ssh yordan12@192.168.1.2
cd EasyRSA-v3.0.6/
./easyrsa import-req /tmp/vpnclient1.req vpnclient1
```

4.7. Then sign the request as you did for the server in the previous step. <br>
This time, though, be sure to specify the client request type: <br>
```
./easyrsa sign-req client vpnclient1
```

4.8. This will create a client certificate file named **vpnclient1.crt**. <br>
Transfer this file back to the server:
```
scp pki/issued/vpnclient1.crt yordan12@192.168.5.2:/tmp
```

4.9. SSH back into your OpenVPN server (apache-pi) and copy the client certificate to the **/client-configs/keys/** directory: <br>
```
cp /tmp/vpnclient1.crt ~/client-configs/keys/
```

4.10. Next, copy the ca.crt and ta.key files to the **/client-configs/keys/** directory as well: <br>
```
cp ~/EasyRSA-v3.0.6/ta.key ~/client-configs/keys/
sudo cp /etc/openvpn/ca.crt ~/client-configs/keys/
```

With that, your server and client’s certificates and keys have all been generated and are stored in the appropriate directories on your server. <br>

#### 5. Step 5 - Configuring the OpenVPN Service
Now that both your client and server’s certificates and keys have been generated, you can begin configuring the OpenVPN service to use these credentials. <br>

5.1. Start by copying a sample OpenVPN configuration file into the configuration directory and then extract it in order to use it as a basis for your setup: <br>
```
sudo cp /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz /etc/openvpn/
sudo gzip -d /etc/openvpn/server.conf.gz
```

5.2. Open the server configuration file in your preferred text editor: <br>
```
sudo nano /etc/openvpn/server.conf
```

5.3. Find the HMAC section by looking for the **tls-auth** directive. This line should already be uncommented, but if isn’t then remove the “;” to uncomment it: <br>
```
tls-auth ta.key 0 # This file is secret
```

5.4. Next, find the section on cryptographic ciphers by looking for the commented out cipher lines. <br>
The AES-256-CBC cipher offers a good level of encryption and is well supported. <br>
Again, this line should already be uncommented, but if it isn’t then just remove the “;” preceding it: <br>
```
cipher AES-256-CBC
```

5.5. Below this, add an auth directive to select the HMAC message digest algorithm. <br>
For this, SHA256 is a good choice: <br>
```
auth SHA256
```

5.6. Next, find the line containing a **dh** directive which defines the Diffie-Hellman parameters. <br>
Because of some recent changes made to EasyRSA, the filename for the Diffie-Hellman key may be different than what is listed in the example server configuration file. <br>
If necessary, change the file name listed here by removing the **2048** so it aligns with the key you generated in the previous step: <br>
```
dh dh.pem
```

5.7. Finally, find the user and group settings and remove the “;” at the beginning of each to uncomment these lines: <br>
```
user nobody
group nogroup
```
Save the configuration. <br>

5.8. (Optional) Push DNS Changes to Redirect All Traffic Through the VPN <br>
The settings above will create the VPN connection between the two machines, but will not force any connections to use the tunnel. <br>
If you wish to use the VPN to route all of your traffic, you will likely want to push the DNS settings to the client computers. <br>

5.8.1. Find the redirect-gateway section and remove the semicolon “;” from the beginning of the redirect-gateway line to uncomment it: <br>
```
push "redirect-gateway def1 bypass-dhcp"
```

5.8.2. Just below this, find the dhcp-option section. Again, remove the “;” from in front of both of the lines to uncomment them: <br>
```
push "dhcp-option DNS 208.67.222.222"
push "dhcp-option DNS 208.67.220.220"
```

This will assist clients in reconfiguring their DNS settings to use the VPN tunnel for as the default gateway. <br>

#### 6. Step 6 - Adjusting the Server Networking Configuration
There are some aspects of the server’s networking configuration that need to be tweaked so that OpenVPN can correctly route traffic through the VPN. <br>

6.1. Adjust your server’s default IP forwarding setting by modifying the **/etc/sysctl.conf** file: <br>
```
sudo nano /etc/sysctl.conf
```

6.2. Inside, look for the commented line that sets **net.ipv4.ip_forward**. <br>
Remove the “#” character from the beginning of the line to uncomment this setting: <br>
```
/etc/sysctl.conf
net.ipv4.ip_forward=1
```
Save and close the file when you are finished. <br>

6.3. To read the file and adjust the values for the current session, type: <br>
```
sudo sysctl -p
```

#### 7. Step 7 - Starting and Enabling the OpenVPN Service
You are finally ready to start the OpenVPN service on your server. This is done using the systemd utility **systemctl**. <br>

7.1. Start the OpenVPN server by specifying your configuration file name as an instance variable after the systemd unit file name. <br>
The configuration file for your server is called: **/etc/openvpn/server.conf**. <br>
So add @server to end of your unit file when calling it: <br>
```
sudo systemctl start openvpn@server
```

7.2. Double-check that the service has started successfully by typing: <br>
```
sudo systemctl status openvpn@server
```
<img src="images/18-b-systemctl-status-openvpn.png" width="700"> <br>

7.3. You can also check that the OpenVPN tun0 interface is available by typing: <br>
```
ip addr show tun0
```
<img src="images/18-c-ip-addr-show-tun0.png" width="700"> <br>

7.4. After starting the service, enable it so that it starts automatically at boot: <br>
```
sudo systemctl enable openvpn@server
```

Your OpenVPN service is now up and running. Before you can start using it, though, you must first create a configuration file for the client machine. <br>

#### 8. Step 8 - Creating the Client Configuration Infrastructure
Rather than writing a single configuration file that can only be used on one client, this step outlines a process for building a client configuration infrastructure which you can use to generate config files on-the-fly. You will first create a “base” configuration file then build a script which will allow you to generate unique client config files, certificates, and keys as needed. <br>
8.1. Get started by creating a new directory where you will store client configuration files within the client-configs directory you created earlier: <br>
```
mkdir -p ~/client-configs/files
```

8.2. Next, copy an example client configuration file into the client-configs directory to use as your base configuration: <br>
```
cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf ~/client-configs/base.conf
```

8.3. Open this new file in your text editor: <br>
```
nano ~/client-configs/base.conf
```

8.4. Inside, locate the remote directive. <br>
This points the client to your OpenVPN server address — the public IP address of your OpenVPN server.  <br>
If you decided to change the port that the OpenVPN server is listening on, you will also need to change 1194 to the port you selected: <br>
```
~/client-configs/base.conf
. . .
# The hostname/IP and port of the server.
# You can have multiple remote entries
# to load balance between the servers.
remote 192.168.5.2 1194
. . .
```

8.5. Next, uncomment the user and group directives by removing the “;” at the beginning of each line: <br>
```
~/client-configs/base.conf
# Downgrade privileges after initialization (non-Windows only)
user nobody
group nogroup
```

8.6. Find the directives that set the ca, cert, and key.  <br>
Comment out these directives since you will add the certs and keys within the file itself shortly: <br>
```
~/client-configs/base.conf
# SSL/TLS parms.
# See the server config file for more
# description.  It's best to use
# a separate .crt/.key file pair
# for each client.  A single ca
# file can be used for all clients.
#ca ca.crt
#cert client.crt
#key client.key
```

8.7. Similarly, comment out the tls-auth directive, as you will add ta.key directly into the client configuration file: <br>
```
~/client-configs/base.conf
# If a tls-auth key is used on the server
# then every client must also have the key.
#tls-auth ta.key 1
```

8.8. Mirror the cipher and auth settings that you set in the **/etc/openvpn/server.conf** file: <br>
```
~/client-configs/base.conf
cipher AES-256-CBC
auth SHA256
```

8.9. Next, add the key-direction directive somewhere in the file.  <br>
You must set this to “1” for the VPN to function correctly on the client machine: <br>
```
~/client-configs/base.conf
key-direction 1
```

8.10. Finally, add a few commented out lines. <br>
Although you can include these directives in every client configuration file, you only need to enable them for Linux clients that ship with an **/etc/openvpn/update-resolv-conf** file. <br>
This script uses the resolvconf utility to update DNS information for Linux clients. <br>
```
~/client-configs/base.conf
# script-security 2
# up /etc/openvpn/update-resolv-conf
# down /etc/openvpn/update-resolv-conf
```
If your client is running Linux and has an **/etc/openvpn/update-resolv-conf** file, uncomment these lines from the client’s configuration file after it has been generated. <br>
Save and close the file when you are finished. <br>

8.11. Next, create a simple script that will compile your base configuration with the relevant certificate, key, and encryption files and then place the generated configuration in the **~/client-configs/files** directory. <br>
Open a new file called make_config.sh within the **~/client-configs** directory:
```
nano ~/client-configs/make_config.sh
```
Inside, add the following content: <br>
```
~/client-configs/make_config.sh
#!/bin/bash

# First argument: Client identifier

KEY_DIR=/home/yordan12/client-configs/keys
OUTPUT_DIR=/home/yordan12/client-configs/files
BASE_CONFIG=/home/yordan12/client-configs/base.conf

cat ${BASE_CONFIG} \
    <(echo -e '<ca>') \
    ${KEY_DIR}/ca.crt \
    <(echo -e '</ca>\n<cert>') \
    ${KEY_DIR}/${1}.crt \
    <(echo -e '</cert>\n<key>') \
    ${KEY_DIR}/${1}.key \
    <(echo -e '</key>\n<tls-auth>') \
    ${KEY_DIR}/ta.key \
    <(echo -e '</tls-auth>') \
    > ${OUTPUT_DIR}/${1}.ovpn
```
Save and close the file when you are finished. <br>

8.12. Before moving on, be sure to mark this file as executable by typing: <br>
```
chmod 700 ~/client-configs/make_config.sh
```

#### 9. Step 9 - Generating Client Configurations
9.1. You can generate a config file for these credentials by moving into your **~/client-configs** directory and running the script you made at the end of the previous step: <br>
```
cd ~/client-configs
sudo ./home/yordan12/client-configs/make_config.sh vpnclient1
```

9.2. You need to transfer this file to the device you plan to use as the client. <br>
For instance, this could be your local computer or a mobile device (using SCP or SFTP). <br>

#### 10. Step 10 - Installing the Client Configuration
##### 10.1. Windows
###### A. Installing in Windows
10.1.1. Download the OpenVPN client application for Windows from OpenVPN’s Downloads page: https://openvpn.net/community-downloads/ <br>
Choose the appropriate installer version for your version of Windows. <br>
<img src="images/18-d-windows-inst.png" width="500"> <br>
Note: OpenVPN needs administrative privileges to install. <br>

10.1.2. . After installing OpenVPN, copy the .ovpn file to: <br>
```
C:\Program Files\OpenVPN\config
```

When you launch OpenVPN, it will automatically see the profile and make it available. <br>

10.1.3. You must run OpenVPN as an administrator each time it’s used, even by administrative accounts. <br>
To set the OpenVPN application to always run as an administrator, right-click on its shortcut icon and go to Properties. <br>
At the bottom of the Compatibility tab, click the button to Change settings for all users. <br>
In the new window, check Run this program as an administrator. <br>
<img src="images/18-e-windows-properties.png" width="300"> <br>

###### B. Connecting in Windows
Each time you launch the OpenVPN GUI, Windows will ask if you want to allow the program to make changes to your computer. Click Yes. <br>
Launching the OpenVPN client application only puts the applet in the system tray so that you can connect and disconnect the VPN as needed; it does not actually make the VPN connection. <br>
Once OpenVPN is started, initiate a connection by going into the system tray applet and right-clicking on the OpenVPN applet icon. This opens the context menu. <br>
Select vpnclient1 at the top of the menu (that’s your vpnclient1.ovpn profile) and choose Connect. <br>

A status window will open showing the log output while the connection is established, and a message will show once the client is connected. <br>
Disconnect from the VPN the same way: Go into the system tray applet, right-click the OpenVPN applet icon, select the client profile and click Disconnect. <br>

##### 10.2. iOS
###### A. Installing in iOS
From the iTunes App Store, search for and install OpenVPN Connect, the official iOS OpenVPN client application. To transfer your iOS client configuration onto the device, connect it directly to a computer. <br>

The process of completing the transfer with iTunes is outlined here: <br>
10.2.1. Open iTunes on the computer and click on iPhone > apps. <br>

10.2.2. Scroll down to the bottom to the File Sharing section and click the OpenVPN app. <br>

10.2.3. The blank window to the right, OpenVPN Documents, is for sharing files. <br>

10.2.4. Drag the .ovpn file to the OpenVPN Documents window. <br>
<img src="images/18-f-ios.png" width="700"> <br>

10.2.5. Now launch the OpenVPN app on the iPhone. You will receive a notification that a new profile is ready to import. Tap the green plus sign to import it. <br>
<img src="images/18-g-ios-1.png" width="250">
<img src="images/18-g-ios-2.png" width="250">
<img src="images/18-g-ios-3.png" width="250"> <br>

###### B. Connecting in iOS
OpenVPN is now ready to use with the new profile. Start the connection by sliding the Connect button to the On position. Disconnect by sliding the same button to Off. <br>
Note: The VPN switch under Settings cannot be used to connect to the VPN. If you try, you will receive a notice to only connect using the OpenVPN app. <br>
For testing, open your browser and open https://www.dnsleaktest.com/ <br>

#### 11. Setup NAT On The OpenVPN Server
When you create a VPN connection between your client and VPN server, a private network is formed between the too, with address starting with 192.168.x.x, 10.x.x.x or 172.16.x.x. <br>
When you want to route traffic from the VPN client to the global Internet, you must use NAT on the server so that it translates the VPN client's private network address to the server's public IP address. <br>
This is independent of the fact if your client's connection is behind NAT or not. <br>
So, in addition to installing the VPN software, you need to add firewall rules for NAT in your server. <br>

##### 11.1. Install A Firewall In The OpenVPN Server
Ufw stands for 'Uncomplicated Fire Wall'. This is the default firewall tool in Ubuntu, and can be easily installed on your Raspberry Pi: <br>
```
sudo apt install ufw
sudo ufw app list

sudo ufw allow ssh
sudo ufw allow OpenSSH
sudo ufw limit ssh/tcp
sudo ufw allow WWW
sudo ufw allow VNC
sudo ufw allow 443
sudo ufw allow 161
sudo ufw allow 5901

sudo ufw enable
sudo ufw status
```
<img src="images/18-h-ufw-status.png" width="400"> <br>

##### 11.2. Find The Public Network Interface Of Your Machine
Before opening the firewall configuration file to add the masquerading rules, you must first find the public network interface of your machine. <br>
To do this, type: <br>
```
ip route | grep default
```
<img src="images/18-i-ip-route.png" width="500"> <br>
Your public interface is the string found within this command’s output that follows the word **dev**. For example, this result shows the interface named wlan0. <br>

##### 11.3. Open The /etc/ufw/before.rules
When you have the interface associated with your default route, open the /etc/ufw/before.rules file to add the relevant configuration: <br>
```
sudo nano /etc/ufw/before.rules
```

UFW rules are typically added using the ufw command. Rules listed in the **before.rules** file, though, are read and put into place before the conventional UFW rules are loaded. <br>
Towards the top of the file, add the lines below. This will set the default policy for the POSTROUTING chain in the NAT table and masquerade any traffic coming from the VPN. <br>
Remember to replace wlan0 in the -A POSTROUTING line below with the interface you found in the above command: <br>
```
  /etc/ufw/before.rules

#
# rules.before
#
# Rules that should be run before the ufw command line added rules. Custom
# rules should be added to one of these chains:
#   ufw-before-input
#   ufw-before-output
#   ufw-before-forward
#

# START OPENVPN RULES
# NAT table rules
*nat
:POSTROUTING ACCEPT [0:0]
# Allow traffic from OpenVPN client to wlan0 (change to the interface you discovered!)
-A POSTROUTING -s 10.8.0.0/8 -o wlan0 -j MASQUERADE
COMMIT
# END OPENVPN RULES

# Don't delete these required lines, otherwise there will be errors
*filter
```
<img src="images/18-j-before-rules.png" width="700"> <br>

Save and close the file when you are finished. <br>

##### 11.4. Tell UFW To Allow Forwarded Packets By Default
Next, you need to tell UFW to allow forwarded packets by default as well. To do this, open the /etc/default/ufw file: <br>
```
sudo nano /etc/default/ufw
```

Inside, find the **DEFAULT_FORWARD_POLICY** directive and change the value from **DROP** to **ACCEPT**: <br>
```
/etc/default/ufw
DEFAULT_FORWARD_POLICY="ACCEPT"
```
Save and close the file when you are finished. <br>

##### 11.5. Adjust The UFW To Allow Traffic To OpenVPN
If you did not change the port and protocol in the **/etc/openvpn/server.conf** file, you will need to open up UDP traffic to port 1194. If you modified the port and/or protocol, substitute the values you selected here. <br>
```
sudo ufw allow 1194/udp
```

##### 11.6. Disable And Re-enable UFW
After adding those rules, disable and re-enable UFW to restart it and load the changes from all of the files you’ve modified: <br>
```
sudo ufw disable
sudo ufw enable
```
<img src="images/18-k-ufw-status.png" width="400"> <br>

##### 11.7. Test Connect The OpenVPN Server From Local LAN And Do Traceroute To A Public Server
<img src="images/18-l-tracert.png" width="600"> <br>

#### 12. Edit The vpnclient1-full.ovpn
Edit the vpnclient1-full.ovpn (in both your Windows laptop and your mobile phone). Replace 192.168.5.2 with your public IP address (in my case 73.32.136.102). <br>
```
# The hostname/IP and port of the server.
# You can have multiple remote entries
# to load balance between the servers.
remote 73.32.136.102 1194
;remote my-server-2 1194
```

#### 13. Configure Split Tunnel/Partial VPN Routing
To setup a partial VPN, make a copy of your client configuration **client.ovpn** generated earlier to **vpnclient1-partial.ovpn**, and add the following lines below the word **client** on line 16: <br>
```
client
route 192.168.1.0 255.255.255.0
route 192.168.2.0 255.255.255.0
route 192.168.4.0 255.255.255.0
route 192.168.5.0 255.255.255.0
route-nopull
```

Make sure the subnet and netmask match your home network configuration. <br>
I maintain two configurations: <br>
   - vpnclient1-full.ovpn <br>
   - vpnclient1-partial.ovpn <br>

Test Connect the OpenVPN Server from the internet (e.g. your mobile phone carrier) and do traceroute to a public server (e.g. 4.2.2.2). <br>

## 19. Modify The USG Firewall Rules To Allow More Ports
The Firewall needs to allow: <br>
   - http (tcp/80) to my apache-pi using ipv6: http://ipv6.yordan12.com <br>
   - http (tcp/80) to my apache-pi using ipv4: http://ipv4.yordan12.com <br>
   - OpenVPN (udp/1194) to my apache-pi using ipv4, so that I could: <br>
     - ssh/vnc to my apache-pi <br>
     - ssh/vnc to my pi-hole <br>
     - ssh to my USG <br>
     - http to my pi-hole web admin <br>
     - http to my cacti web admin <br>
     - http to my apache-pi test web page <br>

Since USG is based on EdgeOS, which is based on Vyatta OS, then presumably the interaction between NAT, Routing, and Firewall rule are followed: <br>
<img src="images/19-a-vyatta-nat.png" width="700"> <br>

### 19.1. Enable Port Forwarding To The Apache-pi TCP Port 80
In the Cloud Key Controller, Navigate to Settings > Gateway > Port Forwarding. Then click the ‘Create New Port Forward Rule’. <br>
<img src="images/19-b-port-forward.png" width="500"> <br>

In the Create New Port Forward Rule page: <br>
   Name: HTTP_to_apachepi <br>
   From: Anywhere <br>
   Port: 80 <br>
   Forward IP: 192.168.5.2 <br>
   Forward Port: 80 <br>
   Protocol: TCP <br>
   Enable Logging: No <br>
   Enable Forward Rule: Yes <br>
   Click Apply <br>
   (wait for 1 minute until the rule actually works) <br>
   <img src="images/19-c-http-to-apachepi.png" width="300"> <br>
   <img src="images/19-c-http-to-apachepi2.png" width="700"> <br>

### 19.2. Add IPv6 Firewall Rules To Allow IPv6 tcp/80 To The Apache-pi
In the Cloud Key Controller, Navigate to Settings > Internet Security > Firewall > WAN v6 > Groups. <br>
Click Create New Group. <br>
   Name: apachepi_v6 <br>
   Type: IPv6 Address/Subnet <br>
   Address: 2601:2c3:8580:f1b5:fc65:d3b4:ff59:9c34 <br>
   Click Apply <br>
   <img src="images/19-d-apachepiv6-group.png" width="300"> <br>

Click Create New Group <br>
   Name: HTTP <br>
   Type: Port Group <br>
   Port: 80 <br>
   Click Apply <br>
   <img src="images/19-e-http-group.png" width="300"> <br>

Click ‘Create New Rule’ <br>
   Type: WAN v6 In <br>
   Description: HTTP_to_apachepi_v6 <br>
   Enabled: Yes <br>
   Rule Applied: After Predefined Rules <br>
   Action: Accept <br>
   IPv6 Protocol: TCP <br>
   Source <br>
     IPv6 Address Group: Any <br>
     Port Group: Any <br>
   Destination <br>
     IPv6 Address Group: apachepi_v6 <br>
     Port Group: HTTP <br>
	 <img src="images/19-f-rule-created.png" width="300"> <br>
   Click Apply <br>
   (wait for 1 minute until the rule actually works) <br>

Check the Result: <br>
<img src="images/19-g-rule-result.png" width="700"> <br>

### 19.3. Enable Port Forwarding To The Apache-pi UDP Port 1194 (OpenVPN)
In the Cloud Key Controller, Navigate to Settings > Gateway > Port Forwarding. <br>
Then click the ‘Create New Port Forward Rule’. <br>

In the Create New Port Forward Rule page: <br>
   Name: OpenVPN_to_apachepi <br>
   From: Anywhere <br>
   Port: 1194 <br>
   Forward IP: 192.168.5.2 <br>
   Forward Port: 1194 <br>
   Protocol: UDP <br>
   Enable Logging: No <br>
   Enable Forward Rule: Yes <br>
   Click Apply <br>
   (wait for 1 minute until the rule actually works) <br>
   <img src="images/19-h-openvpn-to-apachepi.png" width="300"> <br>
   <img src="images/19-i-rule-result.png" width="500"> <br>

By setting the Port Forwarding Rule, the Firewall Rule is automatically created. <br>
Navigate to Settings > Internet Security > Firewall > WAN <br>
<img src="images/19-j-rule-result.png" width="700"> <br>

### 19.4. Add IPv6 Firewall Rules To Allow IPv6 ICMP To The Apache-pi
In the Cloud Key Controller, Navigate to Settings > Internet Security > Firewall. <br>

Click ‘Create New Rule’ <br>
   Type: WAN v6 In <br>
   Description: ICMP_to_apachepi_ipv6 <br>
   Enabled: Yes <br>
   Rule Applied: After Predefined Rules <br>
   Action: Accept <br>
   IPv6 Protocol: ICMPv6 <br>
   IPv6 ICMP Type Name: Any <br>
   Source <br>
     IPv6 Address Group: Any <br>
     Port Group: Any <br>
   Destination <br>
     IPv6 Address Group: apachepi_v6 <br>
     Port Group: Any <br>
   Click Apply <br>
   (wait for 1 minute until the rule actually works) <br>

### 19.5. Add IPv6 Firewall Rules To Allow IPv6 ICMP To The Pi-Hole
In the Cloud Key Controller, Navigate to Settings > Internet Security > Firewall. <br>
Click Create New Group <br>
   Name: pihole_v6 <br>
   Type: IPv6 Address/Subnet <br>
   Address: 2601:2c3:8580:f1b1:ac06:6918:49a2:9b10 <br>
   Click Apply <br>

In the Cloud Key Controller, Navigate to Settings > Internet Security > Firewall. <br>
   Click ‘Create New Rule’ <br>
   Type: WAN v6 In <br>
   Description: ICMP_to_pihole_ipv6 <br>
   Enabled: Yes <br>
   Rule Applied: After Predefined Rules <br>
   Action: Accept <br>
   IPv6 Protocol: ICMPv6 <br>
   IPv6 ICMP Type Name: Any <br>
   Source <br>
     IPv6 Address Group: Any <br>
     Port Group: Any <br>
   Destination <br>
     IPv6 Address Group: pihole_v6 <br>
     Port Group: Any <br>
   Click Apply <br>
   (wait for 1 minute until the rule actually works) <br>

## 20. Setup DNS In Google Domains
From Google Domains, select your domain, then click Manage > DNS <br>
Name Servers: Use the Google Domains name servers. <br>
DNSSEC: don’t enable yet. <br>
Custom resource records: <br>
   @: ipv4.yordan12.com <br>
   A <br>
   1H <br>
   IPv4 address: 73.32.136.102 (the USG eth0 IPv4 address) <br>
   Click Add <br>

   @: ipv6.yordan12.com <br>
   AAAA <br>
   1H <br>
   IPv6 address: 2601:2c3:8580:f1b5:fc65:d3b4:ff59:9c34 (the apache-pi public IPv6 address) <br>
   Click Add <br>

   @: p0.yordan12.com <br>
   AAAA <br>
   1H <br>
   IPv6 address: 2601:2c3:8580:f1b1:ac06:6918:49a2:9b10 (the Pi-hole public IPv6 address) <br>
   Click Add <br>
<img src="images/20-a-google-domain-setup.png" width="700"> <br>

The new CNAMEs will be propagated to the Internet in less than 15 minutes, although Google Domains said: *The time it takes depends on your domain host. In general, DNS changes are processed and propagated within 48 hours, but sometimes it can take up to 72 hours.*
<img src="images/20-b-webpage.png" width="700"> <br>

## 21. How To Setup HTTPS On Your Webpage
I was using the guideline from this URL: https://pimylifeup.com/raspberry-pi-ssl-lets-encrypt/ <br>

### 21.1. Enable Port Forwarding To The Apache-pi TCP Port 443
In the Cloud Key Controller, Navigate to Settings > Gateway > Port Forwarding. <br>
Then click the ‘Create New Port Forward Rule’. <br>
<img src="images/21-a-https-to-apachepi.png" width="500"> <br>

In the Create New Port Forward Rule page: <br>
   Name: HTTPS_to_apachepi <br>
   From: Anywhere <br>
   Port: 443 <br>
   Forward IP: 192.168.5.2 <br>
   Forward Port: 443 <br>
   Protocol: TCP <br>
   Enable Logging: No <br>
   Enable Forward Rule: Yes <br>
   Click Apply <br>
   (wait for 1 minute until the rule actually works) <br>

### 21.2. Add IPv6 Firewall Rules To Allow IPv6 tcp/443 To The Apache-pi
In the Cloud Key Controller, Navigate to Settings > Internet Security > Firewall > WAN v6 > Groups. <br>

Click Create New Group <br>
   Name: HTTPS <br>
   Type: Port Group <br>
   Port: 443 <br>
   Click Apply <br>

Click Create New Rule <br>
   Type: WAN v6 In <br>
   Description: HTTPS_to_apachepi_v6 <br>
   Enabled: Yes <br>
   Rule Applied: After Predefined Rules <br>
   Action: Accept <br>
   IPv6 Protocol: TCP <br>
   Source <br>
     IPv6 Address Group: Any <br>
     Port Group: Any <br>
   Destination <br>
     IPv6 Address Group: apachepi_v6 <br>
     Port Group: HTTPS <br>
	 Click Apply <br>
     (wait for 1 minute until the rule actually works) <br>

### 21.3. Let’s Encrypt Setup
21.3.1. Before we setup LetsEncrypt on our Raspberry Pi we should first ensure everything is up to date. <br>
```
sudo apt-get update
sudo apt-get upgrade
```

21.3.2. Now we can go ahead and install the actual LetsEncrypt software to our Raspberry Pi by running one of the following commands.`
```
sudo apt-get install python-certbot-apache
```

21.3.3. With Certbot finally installed we can proceed with grabbing an SSL certificate for our Raspberry Pi from Let’s Encrypt. There is a couple of ways of handling this. <br>
If you are using Apache, then the easiest way of grabbing a certificate is by running the command shown below, this will automatically grab and install the certificate into Apache’s configuration.
Before you do that, you will first have to make sure port 80 and port 443 are port forwarded.
```
sudo certbot –apache
# when expire: sudo certbot certonly --standalone -d yordan12.com -d www.yordan12.com
sudo a2enmod ssl
sudo a2ensite default-ssl
```

21.3.4. Make The Apache Server Listen on IPv6
```
sudo nano /etc/apache2/ports.conf

# If you just change the port or add more ports here, you will likely also
# have to change the VirtualHost statement in
# /etc/apache2/sites-enabled/000-default.conf

Listen 80

<IfModule mod_ssl.c>
        Listen 443
</IfModule>

#<IfModule ssl_module>
#       Listen 443
#</IfModule>

<IfModule mod_gnutls.c>
        Listen 443
</IfModule>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
```

21.3.4. Utilizing the standalone built-in web server is incredibly easy, though first, you will have to make sure your port 80 is unblocked and forwarded. <br>
```
certbot certonly --standalone -d yordan12.com -d www.yordan12.com
```

21.3.5. After running these commands, you will be prompted to enter some details, such as your email address. These details are required for Let’s Encrypt to keep track of the certificates it provides and also allow them to contact you if any issues arrive with the certificate. <br>
Once you have filled out the required information, it will proceed to grab the certificate from Let’s Encrypt. <br>
If you run into any issues make sure you have a valid domain name pointing at your IP, make sure port 80 and port 443 are unblocked. <br>
The certificates that are grabbed by the certbot client will be stored in the following folder: <br>
```
/etc/letsencrypt/live/example.com/
```
You will find both the full chain file (fullchain.pem) and the certificate’s private key file (privkey.pem) within these folders. <br>
Make sure you don’t allow others to access these files as they are what keep your SSL connection secure and identify it as a legitimate connection. <br>

## 22. Install Fail2Ban
I was using the guidelines from this URLs: https://raspberrytips.com/security-tips-raspberry-pi/ and https://www.raspberrypi.org/documentation/configuration/security.md <br>
Fail2ban is a tool to detect brute-force attacks and block them. Fail2ban will block attackers IP if they fail to login more than X times. You can configure the number of tries before a ban, and the ban duration. <br>
Follow these steps to install Fail2ban on your Raspberry Pi: <br>

1. Install the package <br>
```
sudo apt install fail2ban
```

2. By default fail2ban will ban attacker 10min after 5 failures. <br>
If you want to change this, all the configuration is in the **/etc/fail2ban** folder mainly in **/etc/fail2ban/jail.conf** <br>

3. Restart the service if you made any changes <br>
```
sudo service fail2ban restart
```

This should really slow your attacker. 5 attempts every 10 minutes, it’s 720 tries a day. <br>

## 23. Backup Both Raspberry Pis' Configuration Once A Week
we will be using Win32 Disk Imager: https://sourceforge.net/projects/win32diskimager/ <br>
1. The first thing you need to do is safely shutdown your Raspberry Pi.  Do this at the command line by running the following command: <br>
```
sudo shutdown now
```
   OR
```
sudo systemctl poweroff
```
(wait for a minute)

2. After a few minutes your Raspberry Pi will be shutdown. Disconnect it from power by unplugging the 5V micro-USB cable. You can now safely remove the micro-SD card from the Pi and place it into a USB card reader connected to your PC (or Mac). <br>

3. Next, open Win32 Disk Imager and click the folder icon next to **Image File** and enter the name of the backup file. In my case I chose **20200104-apache-pi.img**, but any name will do. <br>

4. On the far right top side of the imager, click the Device drop-down and select the drive letter that represents your micro-SD card (or the reader it is plugged into). <br>
   - In my case that is the **D:\ drive**, but yours will likely be different.  <br>
   - You can go to My Computer to see a list of all of your drives if you have any concerns or have multiple drives listed and you are not sure which one to choose.  <br>
   - Your Raspberry Pi micro-SD card will most likely be named “boot” on that screen. <br>

5. Now click on the Read button.  Win32 Disk Imager will begin reading the contents of your SD card and writing it to the file name you chose. Be patient, this is probably going to take between 5 and 10 minutes depending on how much data you’ve copied to the Pi’s SD card. <br>
<img src="images/23-a-win32-disk-imager.png" width="500"> <br>

6. One the process is complete, you will be present with a success dialog box.  All is done and you can safely store your backup on your PC, NAS or cloud storage folder for future use. <br>

7. In the future, if you decide to restore your SD card, just follow the same steps. Only this time select the image and click the Write button.  The process will operate in reverse.  <br>
For an even simpler restore we recommend using Balena Etcher. <br>
One thing we hear a lot from people is that their RPi SD card will not mount on their PC when they try to restore it.  This is a common problem. <br>
The fastest way to fix this is to format the micro-SD card before you try to restore data to it. <br>

8. Note: if you can't open the Pi-Hole admin console after the backup process, you can restart your browser as the first step. If this doesn't work, then you need to restart the Pi-Hole lighttpd process.
```
sudo systemctl restart lighttpd
```

## 24. References
https://www.ui.com/unifi-routing/usg <br>
https://dl.ubnt.com/qsg/USG/USG_EN.html <br>
https://dl.ubnt.com/guides/UniFi/UniFi_Controller_V5_UG.pdf <br>
https://help.ubnt.com/hc/en-us/articles/215458888-UniFi-USG-Advanced-Configuration <br>
https://help.ubnt.com/hc/en-us/articles/227129127-UniFi-Methods-for-Capturing-Useful-Debug-Information <br>
https://community.ui.com/questions/IPv6-and-NAT-NPTv6/0f2fa448-7ae3-4ac1-a702-b3b7f568707a <br>
https://buildmedia.readthedocs.org/media/pdf/vyos/latest/vyos.pdf <br>
https://networkjutsu.com/edgeos-cli-introduction/ <br>
https://networkjutsu.com/how-to-configure-edgerouter-lite-part-one/ <br>
https://networkjutsu.com/how-to-configure-edgerouter-lite-part-two/ <br>
https://www.linux.com/tutorials/iptables-rules-ipv6/ <br>
https://www.linux.com/tutorials/building-linux-firewalls-good-old-iptables-part-1/ <br>
https://www.linux.com/tutorials/building-linux-firewalls-good-old-iptables-part-2/ <br>
https://www.tcpdump.org/manpages/tcpdump.1.html <br>
https://www.tcpdump.org/manpages/pcap-filter.7.html <br>
https://hackertarget.com/tcpdump-examples/ <br>
https://blog.cryptoaustralia.org.au/instructions-for-setting-up-pi-hole/ <br>
https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-18-04 <br>
https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-1804 <br>
https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-18-04 <br>
https://bobcares.com/blog/openvpn-server-change-ip-address/ <br>
https://spin.atomicobject.com/2019/04/07/openvpn-raspberry-pi/ <br>
https://rafaelc.org/tech/p/running-a-web-server-while-using-pi-hole/ <br>
https://www.thegeekpub.com/235628/how-to-backup-a-raspberry-pi/ <br>
